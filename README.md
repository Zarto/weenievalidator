1. Build Repo.
2. In the Bin folder add weenies to be valiated to \WeeniesToValidate folder.
3. Run program.


*  Rule Assessments can be reviewed in Global.cs
   Alternatively RuleSets are exported at runtime to the Reports folder.

*  An aggregate report can be compiled (on by default) to the Reports folder which can be indexed at:

   https://docs.google.com/spreadsheets/d/1aI_cUOyncwcWvheKBmG_hLsuWRLYuS3_HKfwIcAgMzA/edit?usp=sharing

**NOTE:** This is not complete validation. It's a compilation of typical oversight items and shortfalls.
