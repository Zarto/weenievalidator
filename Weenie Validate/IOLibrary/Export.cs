﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace IOLibrary
{
    public static class Export
    {
        /// <summary>
        /// Exports a List of Weenies to file with a default path of ExportWeenies.
        /// </summary>
        /// <param name="exportList"> The list of weenies to export.</param>
        public static void ToFileAllWeenies(List<Weenie> exportList)
        {
            if (!Directory.Exists("..\\ExportWeenies"))
            {
                Directory.CreateDirectory("..\\ExportWeenies");
            }

            foreach (Weenie _weenie in exportList)
            {
                string fileName = $"{_weenie.WeenieClassId} - {_weenie.Name}.json";
                string path = $"..\\ExportWeenies\\{fileName}";
                string _weenieString = JsonConvert.SerializeObject(_weenie, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

                File.WriteAllText(path, _weenieString);

            }
        }

        /// <summary>
        /// Exports a List of Weenies to file with a supplied path. If the directory is not found an attempt to create the directory will be made.
        /// </summary>
        /// <param name="exportList">The list of weenies to export.</param>
        /// <param name="path">The directory path to export to IE C:\\examplepath  OR  ..\\examplepath</param>
        public static void ToFileAllWeenies(List<Weenie> exportList, string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            foreach (Weenie _weenie in exportList)
            {
                string fileName = $"{_weenie.WeenieClassId} - {_weenie.Name}.json";
                path = $"{path}\\{fileName}";
                string _weenieString = JsonConvert.SerializeObject(_weenie, Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

                File.WriteAllText(path, _weenieString);

            }

        }

        public static void ToFileString(string exportString, string fileName, string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (IsValidFilename(fileName))
            {
                path = $"{path}\\{fileName}";
                File.WriteAllText(path,exportString);
            }
            else
            {
                throw new ArgumentException("FileName contains invalid characters.");
            }
        }

        private static bool IsValidFilename(string testName)
        {
            Regex containsABadCharacter = new Regex("["
                  + Regex.Escape(new string(Path.GetInvalidPathChars())) + "]");
            if (containsABadCharacter.IsMatch(testName)) 
            {
                return false; 
            }

            return true;
        }
    }
}
