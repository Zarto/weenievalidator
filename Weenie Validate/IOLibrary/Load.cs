﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Lifestoned.DataModel.Gdle;
using Newtonsoft.Json;

namespace IOLibrary
{
    public class Load
    {
        private ConcurrentBag<string> ExceptionStrings { get; set; } = new ConcurrentBag<string>();
        private string _directoryPath;
        private List<Weenie> _weenieList { get; set; } = new List<Weenie>();
        private string CompiledExceptionStrings = "";

        public string LoadTimers { get; set; }
        private string ValidateDirectoryPath(string path)
        {
            if (path.Equals(""))
            {
                if (!Directory.Exists(".\\WeeniesToValidate"))
                {
                    Directory.CreateDirectory(".\\WeeniesToValidate");
                }
                return ".\\WeeniesToValidate";
            }

            if (!Directory.Exists(path))
            {
                throw new Exception("Selected directory path does not exist.");
            }

            return path;
        }
        public List<Weenie> Begin(out string exceptions, string directorypath = "")
        {
            directorypath = ValidateDirectoryPath(directorypath);
            _directoryPath = directorypath; //assumes valid directory path already determined
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            string[] allFilePaths = GetFiles();
            stopwatch.Stop();
            LoadTimers += $"Elapsed Get Files time = {stopwatch.ElapsedMilliseconds}{Environment.NewLine}";

            stopwatch.Restart();
            Dictionary<string, string> allFileContents = allFilePaths.AsParallel().ToDictionary(x => ReadFile(x));
            stopwatch.Stop();
            LoadTimers += $"Elapsed Read Files time = {stopwatch.ElapsedMilliseconds}{Environment.NewLine}";

            stopwatch.Restart();
            _weenieList = allFileContents.AsParallel().Select(jsonString => ConvertFromJson(jsonString.Key, jsonString.Value)).Where(x => x != null).ToList();
            stopwatch.Stop();
            LoadTimers += $"Elapsed Convert Files to Json time = {stopwatch.ElapsedMilliseconds}{Environment.NewLine}";

            //Build Exceptions Report for file read/convert errors.
            if (!ExceptionStrings.IsEmpty)
            {
                foreach (string exception in ExceptionStrings)
                {
                    CompiledExceptionStrings += exception + Environment.NewLine;
                } 
            }
            exceptions = CompiledExceptionStrings;
            return _weenieList;
        }

        private string[] GetFiles()
        {           
            string[] allFiles = null;
            try
            {
                allFiles = Directory.GetFiles(_directoryPath, "*.json", SearchOption.AllDirectories);
            }
            catch (Exception e) //TODO: maybe expand this to catch all exceptions independently...
            {
                string newException = $"{e.InnerException.Message} | {e.InnerException}";
                ExceptionStrings.Add(newException);
            }
            
            return allFiles;
        }

        private string ReadFile(string path)
        {
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(path);
            }
            catch (Exception e)
            {
                ExceptionStrings.Add($"{e.GetBaseException().Message} || Skipping File...{Path.GetFileName(path)}");
            }
            if (jsonString.Equals(""))
            {
                ExceptionStrings.Add($"{Path.GetFileName(path)} is an empty file. Skipping File...");
            }

            return jsonString;
        }

        private Weenie ConvertFromJson(string jsonString, string path)
        {
            if (jsonString.Equals(""))
            {
                return null;
            }

            try
            {
                Weenie weenie = JsonConvert.DeserializeObject<Weenie>(jsonString);
                return weenie;
            }
            catch (Exception e)
            {
                ExceptionStrings.Add($"{e.GetBaseException().Message} || Skipping File...{path}");
                return null;
            }
        }
    }
}
