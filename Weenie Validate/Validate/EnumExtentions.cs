﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Validate
{
	[Flags] public enum CombatMode
	{
		Undefined = 0,
		NonCombat = 1,
		Melee = 2,
		Missile = 4,
		Magic = 8,
		Combat = 14,
		Valid = 15,
		Forced = 2147483647
	}

	[Flags] public enum TargettingTactic
	{
		None = 0,
		Random = 1,
		Focused = 2,
		LastDamager = 4,
		TopDamager = 8,
		Weakest = 16,
		Strongest = 32,
		Nearest = 64
	}

	[Flags]
	public enum PhysicsState
	{
		Static = 1, // 0x1
		Unused = 2, // 0x2
		Ethereal = 4, // 0x4
		Report_Collisions = 8, // 0x8
		Ignore_Collisions = 16, // 0x10
		NoDraw = 32, // 0x20
		Missile = 64, // 0x40
		Pushable = 128, // 0x80
		Align_Path = 256, // 0x100
		Path_Clipped = 512, // 0x200
		Gravity = 1024, // 0x400
		Lighting_On = 2048, // 0x800
		Particle_Emitter = 4096, // 0x1000
		Unused2 = 8192, // 0x2000
		Hidden = 1634, // 0x4000
		Scripted_Collision = 32768, // 0x8000
		Has_Physics_BSP = 65536, // 0x10000
		Inelastic = 131072, // 0x20000 
		Has_Default_Anim = 262144, // 0x40000
		Has_Default_Script = 524288, // 0x80000
		Cloaked = 1048576, // 0x100000
		Report_Collisions_As_Env = 2097152, // 0x200000
		Edge_Slide = 4194304, // 0x400000
		Sledding = 8388608, // 0x800000  
		Frozen = 16777216 // 0x1000000
	}

	public enum CombatUse
	{
		None,
		Melee,
		Missile,
		Ammo,
		Shield,
		Two_Handed
	}
}
