﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Validate
{
    public enum Rule
    {
        NAME_STRING = 1,
        PLURAL_NAME_STRING,
        MAX_STACK_SIZE_INT,
        DAMAGE_TYPE_INT,
        WEAPON_SKILL_INT_MISSING,
        WEAPON_SKILL_INT_OLD,
        ALLOW_GIVE_BOOL,
        PROC_SPELL_DID,
        SPELL_BOOK_SUPPORT_FLOATS,
        SPELL_BOOK_SUPPORT_INTS,
        GENERATOR_TABLE_INTS,
        GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT,
        GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT,
        GENERATOR_TABLE_SLOTS_SEQUENCE,
        GENERATOR_TABLE_PROBABILITY_SEQUENCE,
        NPC_ATTACKABLE_OR_NPK,
        IS_NOT_SET_TO_VENDOR_WEENIETYPE,
        VENDOR_WEENIETYPE_NO_SHOP_ITEMS,
        SHOULD_BE_STUCK,
        GENDER_HERITAGE,
        WEENIETYPE_CREATURE_DAMAGE_RATING,
        EMOTE_TABLE_PROBABILITY_SEQUENCES,
        EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH,
        EMOTE_TABLE_XP_REWARD,
        CREATURE_OR_VENDOR_NO_CREATURE_INT,
        REFUSE_EMOTE_CONTAINS_DIRECT_REWARD,
        BLANK_EMOTETABLE_LISTS,
        JEWELERY_WEENIE_TYPE,
        USE_CREATE_ITEM_DID,
        WEAPON_SKILL_WEAPON_WIELD_REQ,
        CREATURE_BASELINE_VALIDATION,
        COMBAT_USE_INT
        //weapon skill should match skill req weapon skill - expand for casters.
        //critical freq of 1 - crit multi of 1
    }

    public static class Rules
    {
        public static string FetchRuleInfo(Rule rule)
        {
            string output = "";
            switch (rule)
            {
                case Rule.NAME_STRING:
                    output = $"|Rule #{(int)Rule.NAME_STRING} | NAME_STRING (1) is required at a minimum.";
                    break;
                case Rule.PLURAL_NAME_STRING:
                    output = $"|Rule #{(int)Rule.PLURAL_NAME_STRING} | Max Stack Size is defined as a value greater than 1. PLURAL_NAME_STRING(20) is blank. Does this name require a plural alternate?";
                    break;
                case Rule.MAX_STACK_SIZE_INT:
                    output = $"|Rule #{(int)Rule.MAX_STACK_SIZE_INT} | If Max Stack Size is defined it must also be accompanied by STACK_UNIT_ENCUMB_INT (13) and STACK_UNIT_VALUE_INT (15).";
                    break;
                case Rule.DAMAGE_TYPE_INT:
                    output = $"|Rule #{(int)Rule.DAMAGE_TYPE_INT} | Setting a melee weapon's damage requires both DAMAGE_INT (44) and DAMAGE_TYPE_INT (45).";
                    break;
                case Rule.WEAPON_SKILL_INT_MISSING:
                    output = $"|Rule #{(int)Rule.WEAPON_SKILL_INT_MISSING} | WeenieType is set to Missle or Melee/Missile weapon but no WEAPON_SKILL_INT (48) present or vice versa.";
                    break;
                case Rule.WEAPON_SKILL_INT_OLD:
                    output = $"|Rule #{(int)Rule.WEAPON_SKILL_INT_OLD} | Weenie has oboslete WEAPON_SKILL_INT (48) set (Finesse, Light, Heavy, Missle are valid substitutes).";
                    break;
                case Rule.ALLOW_GIVE_BOOL:
                    output = $"|Rule #{(int)Rule.ALLOW_GIVE_BOOL} | ALLOW_GIVE_BOOL (8) must be true if either AI_ACCEPT_EVERYTHING_BOOL (79) is true OR a weenie contains a Give_EmoteCategory (6).";
                    break;
                case Rule.PROC_SPELL_DID:
                    output = $"|Rule #{(int)Rule.PROC_SPELL_DID} | PROC_SPELL_DID (55) requires companion float PROC_SPELL_RATE_FLOAT (156).";
                    break;
                case Rule.SPELL_BOOK_SUPPORT_FLOATS:
                    output = $"|Rule #{(int)Rule.SPELL_BOOK_SUPPORT_FLOATS} | A spellbook on this item expects MANA_RATE_FLOAT (5) (IE -0.025 mana/sec from item).";
                    break;
                case Rule.SPELL_BOOK_SUPPORT_INTS:
                    output = $"|Rule #{(int)Rule.SPELL_BOOK_SUPPORT_INTS} | A spellbook on this item typically expects ITEM_SPELLCRAFT_INT (106), ITEM_CUR_MANA_INT (107), ITEM_MAX_MANA_INT (108).";
                    break;
                case Rule.GENERATOR_TABLE_INTS:
                    output = $"|Rule #{(int)Rule.GENERATOR_TABLE_INTS} | A Generator Table should have IntStats MAX_GENERATED_OBJECTS_INT (81) & INIT_GENERATED_OBJECTS_INT (82).";
                    break;
                case Rule.GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT:
                    output = $"|Rule #{(int)Rule.GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT} | A Generator Table should have REGENERATION_INTERVAL_FLOAT (41). If missing or 0 generator will be treated as a one time spawn.";
                    break;
                case Rule.GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT:
                    output = $"|Rule #{(int)Rule.GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT} | A Generator Table that contains a slot with SCATTER create location requires GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT (43).";
                    break;
                case Rule.GENERATOR_TABLE_SLOTS_SEQUENCE:
                    output = $"|Rule #{(int)Rule.GENERATOR_TABLE_SLOTS_SEQUENCE} | Generator Table slot entries should be sequential. Duplicated slots will conform as a SET within the table.";
                    break;
                case Rule.GENERATOR_TABLE_PROBABILITY_SEQUENCE:
                    output = $"|Rule #{(int)Rule.GENERATOR_TABLE_PROBABILITY_SEQUENCE} | Generator Table probabilities must be sequentially greater than previous entries. IE 0.1, 0.2, 0.3..etc. EXCEPTION if implemented as a set.";
                    break;
                case Rule.NPC_ATTACKABLE_OR_NPK:
                    output = $"|Rule #{(int)Rule.NPC_ATTACKABLE_OR_NPK} | CHECK - Vendors should be unattackable and have NPK status. Most creature weenies with emote tables (but not all) should also be unattackalbe/NPK.";
                    break;
                case Rule.IS_NOT_SET_TO_VENDOR_WEENIETYPE:
                    output = $"|Rule #{(int)Rule.IS_NOT_SET_TO_VENDOR_WEENIETYPE} | CreateList contains items with DESTINATION set as SHOP type. WeenieType should be Vendor or incorrect DESTINATION set.";
                    break;
                case Rule.VENDOR_WEENIETYPE_NO_SHOP_ITEMS:
                    output = $"|Rule #{(int)Rule.VENDOR_WEENIETYPE_NO_SHOP_ITEMS} | WeenieType is set to Vendor but no items in CreateList contain a SHOP destination type.";
                    break;
                case Rule.SHOULD_BE_STUCK:
                    output = $"|Rule #{(int)Rule.SHOULD_BE_STUCK} | Weenie qualifies as type that should be set to stuck. Creatures, Vendors, Portals & Spell Projectiles are applied as true by default but should still be set correctly.";
                    break;
                case Rule.GENDER_HERITAGE:
                    output = $"|Rule #{(int)Rule.GENDER_HERITAGE} | Weenie is a creature or vendor but does not have HERITAGE_GROUP_INT (188) and/or GENDER_INT (113) set.";
                    break;
                case Rule.WEENIETYPE_CREATURE_DAMAGE_RATING:
                    output = $"|Rule #{(int)Rule.WEENIETYPE_CREATURE_DAMAGE_RATING} | Creatures generally should not have a damage rating. PCAP data errantly suggests creatures have a damage rating. This is actually due to a wielded weapon super imposing on the creature's damage.";
                    break;
                case Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES:
                    output = $"|Rule #{(int)Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES} | Emote Table probability sequences must follow an ascending probability (IE 0.1, 0.2, 0.3, 0.4) for all subsets.";
                    break;
                case Rule.EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH:
                    output = $"|Rule #{(int)Rule.EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH} | Emote Table Key and Category ID must match (IE Give Category (6) must match the key grouping of (6).";
                    break;
                case Rule.EMOTE_TABLE_XP_REWARD:
                    output = $"|Rule #{(int)Rule.EMOTE_TABLE_XP_REWARD} | Emote Type AwardXP_EmoteType (2) is shared by fellowship (rare). Did you mean to use AwardNoShareXP_EmoteType (62)?";
                    break;
                case Rule.CREATURE_OR_VENDOR_NO_CREATURE_INT:
                    output = $"|Rule #{(int)Rule.CREATURE_OR_VENDOR_NO_CREATURE_INT} | WeenieType is set to Creature or Vendor but CREATURE_TYPE_INT(2) is not set.";
                    break;
                case Rule.REFUSE_EMOTE_CONTAINS_DIRECT_REWARD:
                    output = $"|Rule #{(int)Rule.REFUSE_EMOTE_CONTAINS_DIRECT_REWARD} | Refuse Emote directly contains a reward without taking turned over item. This could be exploitable!";
                    break;
                case Rule.BLANK_EMOTETABLE_LISTS:
                    output = $"|Rule #{(int)Rule.BLANK_EMOTETABLE_LISTS} | EmoteTable contains a missing List or an empty List. Finish or remove.";
                    break;
                case Rule.JEWELERY_WEENIE_TYPE:
                    output = $"|Rule #{(int)Rule.JEWELERY_WEENIE_TYPE} | Jewelry should be weenie type GENERIC(1).";
                    break;
                case Rule.USE_CREATE_ITEM_DID:
                    output = $"|Rule #{(int)Rule.USE_CREATE_ITEM_DID} | To create an inventory item from this weenie the following must be true WEENIE_TYPE - Gem, USE_CREATE_ITEM_DID(38) must be present. Optionaly to create more than one, USE_CREATE_QUANTITY_INT(269) must be present.";
                    break;
                case Rule.WEAPON_SKILL_WEAPON_WIELD_REQ:
                    output = $"|Rule #{(int)Rule.WEAPON_SKILL_WEAPON_WIELD_REQ} | Weapon Skill and Weapon Wield Req should typically match.";
                    break;
                case Rule.CREATURE_BASELINE_VALIDATION:
                    output = $"|Rule #{(int)Rule.CREATURE_BASELINE_VALIDATION} | Creature weenie is missing standard expected properties.";
                    break;
                case Rule.COMBAT_USE_INT:
                    output = $"|Rule #{(int)Rule.COMBAT_USE_INT} | COMBAT_USE_INT(51) should be present.";
                    break;
                default:
                    output = "Undefined Rule";
                    break;
            }
            return output;
            //output = $"|Rule #{(int)Rule.} | ";
        }
    }
}

