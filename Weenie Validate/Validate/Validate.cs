﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Shared;

namespace Validate
{
    public class Validate
    {
        private Weenie _weenie;
        private List<string> _issues = new List<string>();

        public Validate(Weenie weenie)
        {
            _weenie = weenie;
        }


        public List<string> ValidateWeenie()
        {
            //This is less than efficient, but lets us stay somewhat organized.
            //This containerizes our rules which is more important than efficiency for this use case.

            //basic checks
            if (_weenie.IntStats == null)
            {
                _issues.Add("IntStats are empty. Is this intentional?");
            }
            if (_weenie.StringStats == null)
            {
                _issues.Add($"StringStats are empty. NAME_STRING (1) is required at a minimum.");
            }

            NAME_STRING();
            PLURAL_NAME_STRING();
            MAX_STACK_SIZE_INT();
            DAMAGE_TYPE_INT();
            WEAPON_SKILL_INT_MISSING();
            WEAPON_SKILL_INT_OLD();
            ALLOW_GIVE_BOOL();
            PROC_SPELL_DID();
            SPELL_BOOK_SUPPORT_FLOATS();
            SPELL_BOOK_SUPPORT_INTS();
            GENERATOR_TABLE_INTS();
            GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT();
            GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT();
            GENERATOR_TABLE_SLOTS_SEQUENCE();
            GENERATOR_TABLE_PROBABILITY_SEQUENCE();
            NPC_ATTACKABLE_OR_NPK();
            IS_NOT_SET_TO_VENDOR_WEENIETYPE();
            VENDOR_WEENIETYPE_NO_SHOP_ITEMS();
            SHOULD_BE_STUCK();
            GENDER_HERITAGE();
            WEENIETYPE_CREATURE_DAMAGE_RATING();
            EMOTE_TABLE_PROBABILITY_SEQUENCES();
            EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH();
            EMOTE_TABLE_XP_REWARD();
            CREATURE_OR_VENDOR_NO_CREATURE_INT();
            REFUSE_EMOTE_CONTAINS_DIRECT_REWARD();
            BLANK_EMOTETABLE_LISTS();
            JEWELERY_WEENIE_TYPE();
            USE_CREATE_ITEM_DID();
            WEAPON_SKILL_WEAPON_WIELD_REQ();
            CREATURE_BASELINE_VALIDATION();
            COMBAT_USE_INT();

            return _issues;
        }

        private void COMBAT_USE_INT()
        {
            if (_weenie.WeenieType_Binder == WeenieType.MeleeWeapon ||
                _weenie.WeenieType_Binder == WeenieType.Missile ||
                _weenie.WeenieType_Binder == WeenieType.MissileLauncher ||
                _weenie.WeenieType_Binder == WeenieType.Ammunition)
            {
                var combatUseInt = _weenie.IntStats?.FirstOrDefault(x => x.Key == (int)IntPropertyId.CombatUse);
                if (combatUseInt == null)
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.COMBAT_USE_INT));
                }
                else
                {
                    string issue = "";
                    switch (_weenie.WeenieType_Binder)
                    {
                        case WeenieType.MeleeWeapon:
                            {
                                if (_weenie.IntStats?.Any(x => x.Key == (int)IntPropertyId.WeaponSkill && x.Value == (int)SkillId.TwoHandedCombat) ?? false)
                                {
                                    if (combatUseInt.Value != (int)CombatUse.Two_Handed)
                                    {
                                        issue = $"  Expected Value: Two_Handed    Current Value: {(CombatUse)combatUseInt.Value}";
                                    }
                                }
                                else
                                {
                                    if (combatUseInt.Value != (int)CombatUse.Melee)
                                    {
                                        issue = $"  Expected Value: Melee    Current Value: {(CombatUse)combatUseInt.Value}";
                                    }
                                }
                            }
                            break;
                        case WeenieType.Missile:
                        case WeenieType.MissileLauncher:
                            if (combatUseInt.Value != (int)CombatUse.Missile)
                            {
                                issue = $"  Expected Value: Missile    Current Value: {(CombatUse)combatUseInt.Value}";
                            }
                            break;
                        case WeenieType.Ammunition:
                            if (combatUseInt.Value != (int)CombatUse.Ammo)
                            {
                                issue = $"  Expected Value: Ammo    Current Value: {(CombatUse)combatUseInt.Value}";
                            }
                            break;
                        default:
                            break;
                    }
                    if (issue != "")
                    {
                        _issues.Add(Rules.FetchRuleInfo(Rule.COMBAT_USE_INT) + Environment.NewLine + issue);
                    }
                }
            }

            if (_weenie.WeenieType_Binder == WeenieType.Generic &&
                _weenie.IntStats.Any(x => x.Key == (int)IntPropertyId.ItemType && x.Value == (int)ItemType.Armor))
            {
                //determine if this piece of armor is a shield since weenie and item type don't explicitly define it.
                bool isShieldLocation = _weenie.IntStats?.Any(x => x.Key == (int)IntPropertyId.ValidLocations && x.Value == (int)EquipMask.Shield) ?? false;
                if (isShieldLocation||
                    _weenie.Name.Contains("shield", StringComparison.OrdinalIgnoreCase))
                {
                    var combatUseInt = _weenie.IntStats?.FirstOrDefault(x => x.Key == (int)IntPropertyId.CombatUse);
                    if (combatUseInt == null)
                    {
                        _issues.Add(Rules.FetchRuleInfo(Rule.COMBAT_USE_INT));
                    }
                    else
                    {
                        if (combatUseInt.Value != (int)CombatUse.Shield)
                        {
                            _issues.Add($"{Rules.FetchRuleInfo(Rule.COMBAT_USE_INT)}{Environment.NewLine}" +
                                $"Expected Value: Shield    Current Value: {(CombatUse)combatUseInt.Value}");
                        }
                    }
                }
            }
        }

        private void CREATURE_BASELINE_VALIDATION()
        {
            if (_weenie.WeenieType_Binder == WeenieType.Creature)
            {
                bool isNpc = _weenie.IntStatTryGet(IntPropertyId.PlayerKillerStatus, true, 16);

                List<DoublePropertyId> requiredFloats = null;
                List<IntPropertyId> requiredInts = null;
                List<DidPropertyId> requiredDids = null;
                List<BoolPropertyId> requiredBools = null;

                #region Monster Stats
                if (!isNpc)
                {
                    //Expected Float Stats
                    requiredFloats = new List<DoublePropertyId>{
                        DoublePropertyId.HealthRate,
                        DoublePropertyId.StaminaRate,
                        DoublePropertyId.ManaRate,
                        DoublePropertyId.ArmorModVsAcid,
                        DoublePropertyId.ArmorModVsBludgeon,
                        DoublePropertyId.ArmorModVsCold,
                        DoublePropertyId.ArmorModVsElectric,
                        DoublePropertyId.ArmorModVsFire,
                        DoublePropertyId.ArmorModVsPierce,
                        DoublePropertyId.ArmorModVsSlash,
                        DoublePropertyId.VisualAwarenessRange,
                        DoublePropertyId.PowerupTime,
                        DoublePropertyId.ChargeSpeed,
                        DoublePropertyId.ResistAcid,
                        DoublePropertyId.ResistBludgeon,
                        DoublePropertyId.ResistCold,
                        DoublePropertyId.ResistElectric,
                        DoublePropertyId.ResistFire,
                        DoublePropertyId.ResistPierce,
                        DoublePropertyId.ResistSlash,
                        DoublePropertyId.ResistHealthDrain,
                        DoublePropertyId.ResistStaminaDrain,
                        DoublePropertyId.ResistManaDrain
                    };

                    //Expected Int Stats
                    requiredInts = new List<IntPropertyId> {
                        IntPropertyId.ItemType,
                        IntPropertyId.CreatureType,
                        IntPropertyId.ItemsCapacity,
                        IntPropertyId.ContainersCapacity,
                        IntPropertyId.ItemUseable,
                        IntPropertyId.Level,
                        IntPropertyId.ArmorType,
                        IntPropertyId.TargetingTactic,
                        IntPropertyId.PhysicsState,
                        IntPropertyId.ShowableOnRadar,
                        IntPropertyId.XpOverride
                     };

                    //Expected Did Stats
                    requiredDids = new List<DidPropertyId> {
                        DidPropertyId.Setup,
                        DidPropertyId.SoundTable,
                        DidPropertyId.MotionTable,
                        DidPropertyId.CombatTable,
                        DidPropertyId.PhysicsEffectTable
                    };

                    //Expected Bool Stats
                    requiredBools = new List<BoolPropertyId> {
                        BoolPropertyId.Stuck,
                        BoolPropertyId.Ethereal,
                        BoolPropertyId.ReportCollisions,
                        BoolPropertyId.IgnoreCollisions
                    };
                }
                #endregion

                #region NPCStats
                else if (isNpc)
                {
                    //Expected Float Stats
                    requiredFloats = new List<DoublePropertyId>{
                         DoublePropertyId.UseRadius
                         };

                    //Expected Int Stats
                    requiredInts = new List<IntPropertyId> {
                        IntPropertyId.ItemType,
                        IntPropertyId.CreatureType,
                        IntPropertyId.ItemsCapacity,
                        IntPropertyId.ContainersCapacity,
                        IntPropertyId.ItemUseable,
                        IntPropertyId.Level,
                        IntPropertyId.ArmorType,
                        IntPropertyId.PhysicsState,
                        IntPropertyId.RadarBlipColor,
                        IntPropertyId.ShowableOnRadar,
                        IntPropertyId.Gender,
                        IntPropertyId.HeritageGroup,
                        IntPropertyId.PlayerKillerStatus //redundant...but we will leave it for posterity.
                    };

                    //Expected Did Stats
                    requiredDids = new List<DidPropertyId> {
                        DidPropertyId.Setup,
                        DidPropertyId.SoundTable,
                        DidPropertyId.MotionTable,
                        DidPropertyId.PhysicsEffectTable
                     };

                    //Expected Bool Stats
                    requiredBools = new List<BoolPropertyId> {
                        BoolPropertyId.Stuck,
                        BoolPropertyId.Ethereal,
                        BoolPropertyId.ReportCollisions,
                        BoolPropertyId.IgnoreCollisions,
                        BoolPropertyId.Attackable
                     };
                }
                #endregion

                string issues = "";
                int issuesCount = 0;

                //BODY TABLE CHECKS
                //Only check this for Monsters, don't care if we are an unattackable/non-attacking NPC.
                if (!isNpc)
                {
                    if (!_weenie.HasBodyPartList)
                    {
                        issues += $"  WARN: Missing BodyTable.{Environment.NewLine}";
                    }
                    else
                    {
                        double? HLF = 0, MLF = 0, LLF = 0;
                        double? HRF = 0, MRF = 0, LRF = 0;
                        double? HLB = 0, MLB = 0, LLB = 0;
                        double? HRB = 0, MRB = 0, LRB = 0;

                        foreach (BodyPartListing bodyPart in _weenie.Body.BodyParts)
                        {
                            // bh checks

                            if (bodyPart.BodyPart.BH < 0 || bodyPart.BodyPart.BH > 3)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} has invalid bh data {bodyPart.BodyPart.BH}. Default target attack height value should be one of 0, 1, 2, 3 ( Undefined, high, mid, low respectively). NOTE: 0 only seen on breath part in cache.{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.BH == 0 && bodyPart.BodyPartType != BodyPartType.Breath)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} has invalid bh data {bodyPart.BodyPart.BH}. Only breath bodyparts should have a value of 0 (undefined). Even parts with 0 dval " +
                                          $"should have an appropriate default target attack height assigned according to convention.{Environment.NewLine}";
                            }


                            if (bodyPart.BodyPart.DVal > 0)
                            {
                                if (bodyPart.BodyPart.DType == (int)DamageType.Undef)
                                {
                                    issues += $"  WARN: BodyTable {bodyPart.BodyPartType} has damage assigned but do damage type assigned.{Environment.NewLine}";
                                }
                                if (bodyPart.BodyPart.DVar == 0)
                                {
                                    issues += $"  CHECK: BodyTable {bodyPart.BodyPartType} has damage assigned and a zero variance set. Was this intentional?{Environment.NewLine}";
                                }
                            }

                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsSlash, out FloatStat slashArmorMod))
                            {
                                double moddedSlashValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * slashArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedSlashValue != bodyPart.BodyPart.ArmorValues.ArmorVsSlash)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Slash should be {moddedSlashValue} (BaseArmor * floatStat 13) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsSlash}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsPierce, out FloatStat piercArmorMod))
                            {
                                double moddedPierceValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * piercArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedPierceValue != bodyPart.BodyPart.ArmorValues.ArmorVsPierce)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Pierce should be {moddedPierceValue} (BaseArmor * floatStat 14) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsPierce}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsBludgeon, out FloatStat bludgeArmorMod))
                            {
                                double moddedBludgeonValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * bludgeArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedBludgeonValue != bodyPart.BodyPart.ArmorValues.ArmorVsBludgeon)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Bludgeon should be {moddedBludgeonValue} (BaseArmor * floatStat 15) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsBludgeon}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsCold, out FloatStat coldArmorMod))
                            {
                                double moddedColdValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * coldArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedColdValue != bodyPart.BodyPart.ArmorValues.ArmorVsCold)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Cold should be {moddedColdValue} (BaseArmor * floatStat 16) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsCold}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsFire, out FloatStat fireArmorMod))
                            {
                                double moddedFireValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * fireArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedFireValue != bodyPart.BodyPart.ArmorValues.ArmorVsFire)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Fire should be {moddedFireValue} (BaseArmor * floatStat 17) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsFire}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsAcid, out FloatStat acidArmorMod))
                            {
                                double moddedAcidValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * acidArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedAcidValue != bodyPart.BodyPart.ArmorValues.ArmorVsAcid)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Acid should be {moddedAcidValue} (BaseArmor * floatStat 18) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsAcid}{Environment.NewLine}";
                                }
                            }
                            if (_weenie.FloatStatTryGet(DoublePropertyId.ArmorModVsElectric, out FloatStat electricArmorMod))
                            {
                                double moddedElectricValue = Math.Round(bodyPart.BodyPart.ArmorValues.BaseArmor * electricArmorMod.Value, MidpointRounding.AwayFromZero);
                                if (moddedElectricValue != bodyPart.BodyPart.ArmorValues.ArmorVsElectric)
                                {
                                    issues += $"  BodyTable {bodyPart.BodyPartType} - armor Vs Electric should be {moddedElectricValue} (BaseArmor * floatStat 19) Current value: {bodyPart.BodyPart.ArmorValues.ArmorVsElectric}{Environment.NewLine}";
                                }
                            }
                            if (bodyPart.BodyPart.ArmorValues.ArmorVsNether > 0)
                            {
                                issues += $"  BodyTable nether armor value is greater than 0.{Environment.NewLine}";
                            }

                            HLF += bodyPart.BodyPart.SD.HLF;
                            MLF += bodyPart.BodyPart.SD.MLF;
                            LLF += bodyPart.BodyPart.SD.LLF;

                            HRF += bodyPart.BodyPart.SD.HRF;
                            MRF += bodyPart.BodyPart.SD.MRF;
                            LRF += bodyPart.BodyPart.SD.LRF;

                            HLB += bodyPart.BodyPart.SD.HLB;
                            MLB += bodyPart.BodyPart.SD.MLB;
                            LLB += bodyPart.BodyPart.SD.LLB;

                            HRB += bodyPart.BodyPart.SD.HRB;
                            MRB += bodyPart.BodyPart.SD.MRB;
                            LRB += bodyPart.BodyPart.SD.LRB;

                            //LF
                            if (bodyPart.BodyPart.SD.HLF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} HIGH LEFT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.MLF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} MID LEFT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.LLF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} LOW LEFT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }

                            //RF
                            if (bodyPart.BodyPart.SD.HRF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} HIGH RIGHT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.MRF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} MID RIGHT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.LRF == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} LOW RIGHT FRONT has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }

                            //LB
                            if (bodyPart.BodyPart.SD.HLB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} HIGH LEFT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.MLB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} MID LEFT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.LLB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} LOW LEFT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }

                            //RB
                            if (bodyPart.BodyPart.SD.HRB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} HIGH RIGHT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.MRB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} MID RIGHT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                            if (bodyPart.BodyPart.SD.LRB == 1.0)
                            {
                                issues += $"  WARN: BodyTable {bodyPart.BodyPartType} LOW RIGHT BACK has value of 1.0 (no single part should be 1.0).{Environment.NewLine}";
                            }
                        }

                        //LEFT FRONT
                        if (HLF > 1.001 || 1.0 - HLF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for HIGH LEFT FRONT should sum to 1.0 across all bodyparts. Current sum {HLF}{Environment.NewLine}";
                        }
                        if (MLF > 1.001 || 1.0 - MLF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for MID LEFT FRONT should sum to 1.0 across all bodyparts. Current sum {MLF}{Environment.NewLine}";
                        }
                        if (LLF > 1.001 || 1.0 - LLF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for LOW LEFT FRONT should sum to 1.0 across all bodyparts. Current sum {LLF}{Environment.NewLine}";
                        }

                        //RIGHT FRONT
                        if (HRF > 1.001 || 1.0 - HRF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for HIGH RIGHT FRONT should sum to 1.0 across all bodyparts. Current sum {HRF}{Environment.NewLine}";
                        }
                        if (MRF > 1.001 || 1.0 - MRF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for MID RIGHT FRONT should sum to 1.0 across all bodyparts. Current sum {MRF}{Environment.NewLine}";
                        }
                        if (LRF > 1.001 || 1.0 - LRF > 0.001)
                        {
                            issues += $"  BodyTable targeting data for LOW RIGHT FRONT should sum to 1.0 across all bodyparts. Current sum {LRF}{Environment.NewLine}";
                        }

                        //LEFT BACK
                        if (HLB > 1.001 || 1.0 - HLB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for HIGH LEFT BACK should sum to 1.0 across all bodyparts. Current sum {HLB}{Environment.NewLine}";
                        }
                        if (MLB > 1.001 || 1.0 - MLB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for MID LEFT BACK should sum to 1.0 across all bodyparts. Current sum {MLB}{Environment.NewLine}";
                        }
                        if (LLB > 1.001 || 1.0 - LLB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for LOW LEFT BACK should sum to 1.0 across all bodyparts. Current sum {LLB}{Environment.NewLine}";
                        }

                        //RIGHT BACK
                        if (HRB > 1.001 || 1.0 - HRB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for HIGH RIGHT BACK should sum to 1.0 across all bodyparts. Current sum {HRB}{Environment.NewLine}";
                        }
                        if (MRB > 1.001 || 1.0 - MRB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for MID RIGHT BACK should sum to 1.0 across all bodyparts. Current sum {MRB}{Environment.NewLine}";
                        }
                        if (LRB > 1.001 || 1.0 - LRB > 0.001)
                        {
                            issues += $"  BodyTable targeting data for LOW RIGHT BACK should sum to 1.0 across all bodyparts. Current sum {LRB}{Environment.NewLine}";
                        }
                    }
                }

                //SKILLS CHECKS
                issuesCount = AddStringPadding(issuesCount, ref issues);
                //Only check this for Monsters, don't care if we are an unattackable/non-attacking NPC.
                if (!isNpc)
                {
                    if (!_weenie.HasSkills())
                    {
                        issues += $"  WARN: Missing SkillsTable.{Environment.NewLine}";
                    }
                    else
                    {
                        //First check for minimum skills (defense skills).
                        List<SkillId> minimumExpectedSkillSet = new List<SkillId> { SkillId.MeleeDefense, SkillId.MissileDefense, SkillId.MagicDefense };
                        minimumExpectedSkillSet.ForEach(stat =>
                        {
                            SkillListing skill = _weenie.Skills.FirstOrDefault(x => x.SkillId == (int)stat);
                            if (skill == null || skill.Skill.Ranks == 0 || skill.Skill.TrainedLevel == 0)
                            {
                                string status = "NULL";
                                if (skill != null)
                                {
                                    status = $"SkillLevel: {skill.Skill.Ranks} Specialization Lvl: {skill.Skill.TrainedLevel}";
                                }
                                issues += $"  Missing/Incomplete min skill {stat}({(int)stat})  {status}{Environment.NewLine}";
                            }
                        });

                        //Second make sure we have at least one valid attack skill and those skills are not default.
                        bool hasValidAttackSkill = false;
                        _weenie.Skills.ForEach(skill =>
                        {
                            switch ((SkillId)skill.SkillId)
                            {
                                //REMOVE: Allow Old Skills to pass for now.
                                case SkillId.UnarmedCombat:
                                case SkillId.Sword:
                                case SkillId.Spear:
                                case SkillId.Staff:
                                case SkillId.Dagger:
                                case SkillId.Mace:
                                case SkillId.Crossbow:
                                case SkillId.Bow:
                                //Skills going forward
                                case SkillId.ThrownWeapon:
                                case SkillId.TwoHandedCombat:
                                case SkillId.HeavyWeapons:
                                case SkillId.LightWeapons:
                                case SkillId.FinesseWeapons:
                                case SkillId.MissileWeapons:
                                case SkillId.DualWield:
                                    {
                                        if (skill.Skill.Ranks == 0 || skill.Skill.TrainedLevel == 0)
                                        {
                                            issues += $"  Incomplete skill {(SkillId)skill.SkillId}({skill.SkillId}) SkillLevel: {skill.Skill.Ranks} Specialization Lvl: {skill.Skill.TrainedLevel}{Environment.NewLine}";
                                        }
                                        else
                                        {
                                            hasValidAttackSkill = true;
                                        }
                                        break;
                                    }
                                default:
                                    break;
                            }
                        });

                        if (!hasValidAttackSkill)
                        {
                            issues += $"  Missing at least one valid melee/missile attack skill!{Environment.NewLine}";
                        }

                        //Thirdly if we have a spell table, make sure we at least have a valid casting skill and that the casting skills we do have are valid.
                        if (_weenie.HasSpellTable())
                        {
                            bool hasValidCastingSkill = false;
                            _weenie.Skills.ForEach(skill =>
                            {
                                switch ((SkillId)skill.SkillId)
                                {

                                    case SkillId.LifeMagic:
                                    case SkillId.CreatureEnchantment:
                                    case SkillId.ItemEnchantment:
                                    case SkillId.WarMagic:
                                    case SkillId.VoidMagic:
                                        {
                                            if (skill.Skill.Ranks == 0 || skill.Skill.TrainedLevel == 0)
                                            {
                                                issues += $"  Incomplete skill {(SkillId)skill.SkillId}({skill.SkillId}) SkillLevel: {skill.Skill.Ranks} Specialization Lvl: {skill.Skill.TrainedLevel}{Environment.NewLine}";
                                            }
                                            else
                                            {
                                                hasValidCastingSkill = true;
                                            }
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            });

                            if (!hasValidCastingSkill)
                            {
                                issues += $"  Missing at least one valid casting skill and weenie has SpellTable!{Environment.NewLine}";
                            }
                        }
                    }
                }

                //FLOAT CHECKS
                issuesCount = AddStringPadding(issuesCount, ref issues);

                //Setup some escape testing for double values not valid for certain creatures.  Ignore if we are an NPC.
                bool powerUp_Charge_Awareness = true;

                if (!isNpc && _weenie.IntStatTryGet(IntPropertyId.CreatureType, out IntStat creatureType))
                {
                    switch ((CreatureType)creatureType.Value)
                    {
                        case CreatureType.Invalid:
                        case CreatureType.Rockslide:
                        case CreatureType.Statue:
                        case CreatureType.Wall:
                        case CreatureType.Device:
                        case CreatureType.DarkSarcophagus:
                        case CreatureType.Unknown:
                            powerUp_Charge_Awareness = false;
                            break;

                        default:
                            break;
                    }
                }

                requiredFloats.ForEach(stat =>
                {
                    if (!_weenie.FloatStatTryGet(stat))
                    {
                        if (!powerUp_Charge_Awareness
                            && (stat == DoublePropertyId.VisualAwarenessRange
                            || stat == DoublePropertyId.PowerupTime
                            || stat == DoublePropertyId.ChargeSpeed
                        ))
                        {
                            return;
                        }

                        issues += $"  Missing FLOAT {stat}({(int)stat}){Environment.NewLine}";
                    }
                });

                //INT CHECKS
                issuesCount = AddStringPadding(issuesCount, ref issues);

                bool hasLooksLikeBool = _weenie.BoolStatTryGet(BoolPropertyId.NpcLooksLikeObject, true, 1);
                requiredInts.ForEach(stat =>
                {
                    //We don't care about these if we look like an object. Lets not report that.
                    if (hasLooksLikeBool
                        && (stat == IntPropertyId.Level
                        || stat == IntPropertyId.ArmorType
                        || stat == IntPropertyId.HeritageGroup
                        || stat == IntPropertyId.Gender))
                    {
                        return;
                    }

                    if (!_weenie.IntStatTryGet(stat))
                    {
                        issues += $"  Missing INT {stat}({(int)stat}){Environment.NewLine}";
                    }
                });

                //DID CHECKS
                issuesCount = AddStringPadding(issuesCount, ref issues);

                requiredDids.ForEach(stat =>
                {
                    if (!_weenie.DidStatTryGet(stat))
                    {
                        issues += $"  Missing DID {stat}({(int)stat}){Environment.NewLine}";
                    }
                });

                //BOOL CHECKS
                issuesCount = AddStringPadding(issuesCount, ref issues);

                requiredBools.ForEach(stat =>
                {
                    if (!_weenie.BoolStatTryGet(stat, out BoolStat boolProp))
                    {
                        issues += $"  Missing BOOL {stat}({(int)stat}){Environment.NewLine}";
                    }
                    else
                    {
                        if (stat == BoolPropertyId.Stuck && boolProp.Value == 0)
                        {
                            issues += $"  BoolStat {BoolPropertyId.Stuck}({(int)BoolPropertyId.Stuck}) is set to false! This should be true!{Environment.NewLine}";
                        }
                    }
                });

                //HasAttributes
                issuesCount = AddStringPadding(issuesCount, ref issues);

                if (!_weenie.HasAllAttributes(out string attributeErrors))
                {
                    issues += $"  Missing or invalid attributes detected on weenie.{Environment.NewLine} {attributeErrors}";
                }

                if (issues != "")
                {
                    string NpcOrCreature = isNpc ? "  NPC Type Creature Detected" : "  Monster Type Creature Detected";
                    _issues.Add(Rules.FetchRuleInfo(Rule.CREATURE_BASELINE_VALIDATION) + Environment.NewLine + NpcOrCreature + Environment.NewLine + issues);
                }
            }
        }

        private void WEAPON_SKILL_WEAPON_WIELD_REQ()
        {
            if (_weenie.WeenieType_Binder == WeenieType.MissileLauncher || _weenie.WeenieType_Binder == WeenieType.MeleeWeapon)
            {
                bool hasWeaponSkill = _weenie.IntStatTryGet(IntPropertyId.WeaponSkill, out IntStat weaponSkill);
                bool hasWieldReq = _weenie.IntStatTryGet(IntPropertyId.WieldRequirements, out IntStat wieldReq);
                bool hasWieldSkill = _weenie.IntStatTryGet(IntPropertyId.WieldSkilltype, out IntStat wieldSkill);
                bool hasWieldDiff = _weenie.IntStatTryGet(IntPropertyId.WieldDifficulty, out IntStat wieldDiff);

                if (hasWieldReq && (wieldReq.Value == (int)WieldRequirements.Skill || wieldReq.Value == (int)WieldRequirements.BaseSkill))
                {
                    if (hasWeaponSkill && hasWieldSkill)
                    {
                        if (weaponSkill.Value != wieldSkill.Value)
                        {
                            string differenceIssue = $"  Current Weapon Skill: {(SkillId)weaponSkill.Value}   Current Wield Req: {(SkillId)wieldSkill.Value}";
                            _issues.Add(Rules.FetchRuleInfo(Rule.WEAPON_SKILL_WEAPON_WIELD_REQ) + Environment.NewLine + differenceIssue);
                        }

                        //if ((hasWieldReq && !hasWieldDiff) || (hasWieldReq && wieldDiff.Value == 0)) // Mutations handle alot of the diff value and is a spurious check.
                        if (hasWieldSkill && hasWieldDiff && wieldDiff.Value == 0)
                        {
                            string difficultyValue = "NULL";
                            if (hasWieldDiff)
                            {
                                difficultyValue = wieldDiff.Value.ToString();
                            }
                            string missingDifficulty = $"  WIELD_DIFFICULTY(160) 0 value detected. Current Wield Req: {(SkillId)wieldSkill.Value}  Current Wield Diff: {difficultyValue}";
                            _issues.Add(Rules.FetchRuleInfo(Rule.WEAPON_SKILL_WEAPON_WIELD_REQ) + Environment.NewLine + missingDifficulty);
                        }
                    }
                }
            }
        }

        private void USE_CREATE_ITEM_DID()
        {
            bool hasIntCreateQuantity = _weenie.IntStatTryGet(IntPropertyId.UseCreateQuantity, out IntStat intCreateQuantity);
            bool hasDidCreateItem = _weenie.DidStatTryGet(DidPropertyId.UseCreateItem, out DidStat didCreateItem);
            bool isGemType = _weenie.WeenieType_Binder == WeenieType.Gem;

            if (hasIntCreateQuantity || hasDidCreateItem)
            {
                if (!isGemType || !hasDidCreateItem || !hasIntCreateQuantity)
                {
                    string status = $"  WeeineType: {_weenie.WeenieType_Binder}   USE_CREATE_QUANTITY_INT(269): {hasIntCreateQuantity}  USE_CREATE_ITEM_DID(38): {hasDidCreateItem}";
                    _issues.Add(Rules.FetchRuleInfo(Rule.USE_CREATE_ITEM_DID) + Environment.NewLine + status);
                }
            }
        }

        private void JEWELERY_WEENIE_TYPE()
        {
            if (_weenie.IntStatTryGet(IntPropertyId.ItemType, true, (int)ItemType.Jewelry)
                && _weenie.WeenieType_Binder != WeenieType.Generic)
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.JEWELERY_WEENIE_TYPE));
            }
        }

        private void BLANK_EMOTETABLE_LISTS()
        {
            if (_weenie?.EmoteTable != null)
            {
                string emptyEmoteListExceptions = "";
                foreach (EmoteCategoryListing keyListing in _weenie.EmoteTable)
                {
                    if (keyListing.Emotes == null)
                    {
                        emptyEmoteListExceptions += $"  Missing \"value\" field for key grouping ({keyListing.EmoteCategoryId}){(EmoteCategory)keyListing.EmoteCategoryId}{Environment.NewLine}";
                        continue;
                    }
                    else if (!keyListing.Emotes.Any())
                    {
                        emptyEmoteListExceptions += $"  Empty category list for key grouping ({keyListing.EmoteCategoryId}){(EmoteCategory)keyListing.EmoteCategoryId}{Environment.NewLine}";
                        continue;
                    }

                    foreach (Emote category in keyListing.Emotes)
                    {
                        if (category.Actions == null)
                        {
                            emptyEmoteListExceptions += $"  Missing \"emotes\" field for cateogry grouping ({(int)category.EmoteCategory}){category.EmoteCategory}{Environment.NewLine}";
                        }
                        else if (!category.Actions.Any())
                        {
                            emptyEmoteListExceptions += $"  Empty emote action list for category grouping ({(int)category.EmoteCategory}){category.EmoteCategory}{Environment.NewLine}";
                        }
                    }
                }

                if (emptyEmoteListExceptions != "")
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.BLANK_EMOTETABLE_LISTS) + Environment.NewLine + emptyEmoteListExceptions);
                }
            }
        }

        private void REFUSE_EMOTE_CONTAINS_DIRECT_REWARD()
        {

            //Check to see if there is an award type emote in a refuse category. These are inherently dangerous as they could be exploited without a stamp or inq or take item acompanyment. 
            if (_weenie.EmoteTable != null)
            {
                EmoteCategoryListing refuseCategories = _weenie.EmoteTable.FirstOrDefault(x => x.EmoteCategoryId == (int)EmoteCategory.Refuse);

                if (refuseCategories != null)
                {
                    foreach (var refuseCat in refuseCategories.Emotes)
                    {
                        bool containsAwardType = false;
                        bool containsGiveType = false;
                        bool containsTakeType = false;
                        bool containsStampType = false;
                        uint takeTypeWcid = 0;
                        uint giveTypeId = 0;
                        EmoteType rewardType = EmoteType.Invalid;

                        foreach (EmoteAction action in refuseCat.Actions)
                        {
                            switch (action.EmoteActionType_Binder)
                            {

                                case EmoteType.AwardXP:
                                case EmoteType.AwardSkillXP:
                                case EmoteType.AwardSkillPoints:
                                case EmoteType.AwardTrainingCredits:
                                case EmoteType.AwardLevelProportionalXP:
                                case EmoteType.AwardLevelProportionalSkillXP:
                                case EmoteType.AwardNoShareXP:
                                case EmoteType.AwardLuminance:
                                    rewardType = action.EmoteActionType_Binder;
                                    containsAwardType = true;
                                    break;
                                case EmoteType.TakeItems:
                                    if ((uint)action.Item.WeenieClassId == refuseCat.ClassId)
                                    {
                                        containsTakeType = true;
                                    }
                                    break;
                                case EmoteType.Give:
                                    containsGiveType = true;
                                    giveTypeId = (uint)action.Item.WeenieClassId;
                                    break;
                                case EmoteType.StampQuest:
                                    containsStampType = true;
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (!containsStampType && !containsTakeType)
                        {
                            string specificIssue = "";
                            if (containsAwardType)
                            {
                                specificIssue += $"  RefuseCategory ID: {refuseCat.ClassId}   Contains: {rewardType} with no exploit prevention.{Environment.NewLine}";
                            }

                            if (containsGiveType)
                            {
                                specificIssue += $"  RefuseCategory ID:{refuseCat.ClassId}   Contains GIVE_EMOTE_TYPE with ID: {giveTypeId} with no exploit prevention.{Environment.NewLine}";
                            }

                            if (specificIssue != "")
                            {
                                _issues.Add(Rules.FetchRuleInfo(Rule.REFUSE_EMOTE_CONTAINS_DIRECT_REWARD) + Environment.NewLine + specificIssue);
                            }
                        }
                    }
                }
            }
        }

        private void CREATURE_OR_VENDOR_NO_CREATURE_INT()
        {
            if (_weenie.WeenieType_Binder == WeenieType.Creature || _weenie.WeenieType_Binder == WeenieType.Vendor)
            {
                if (!_weenie.IntStatTryGet(IntPropertyId.CreatureType))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.CREATURE_OR_VENDOR_NO_CREATURE_INT));
                }
            }
        }

        private void NAME_STRING()
        {
            if (_weenie.StringStats != null && !_weenie.StringStatTryGet(StringPropertyId.Name))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.NAME_STRING));
            }
        }

        private void PLURAL_NAME_STRING()
        {
            if (_weenie.IntStats.FirstOrDefault(x => x.Key == (int)IntPropertyId.MaxStackSize && x.Value > 1) != null)
            {
                if (!_weenie.StringStatTryGet(StringPropertyId.PluralName)
                    && _weenie.StringStats.FirstOrDefault(x => x.Key == (int)StringPropertyId.Name && !x.Value.EndsWith("s")) == null)
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.PLURAL_NAME_STRING));
                }
            }
        }

        private void MAX_STACK_SIZE_INT()
        {
            if (_weenie.IntStatTryGet(IntPropertyId.MaxStackSize))
            {
                string stackUnitvalues = "";
                if (!_weenie.IntStatTryGet(IntPropertyId.StackUnitEncumbrance))
                {
                    stackUnitvalues += " Missing STACK_UNIT_ENCUMB_INT (13).";
                }

                if (!_weenie.IntStatTryGet(IntPropertyId.StackUnitValue))
                {
                    stackUnitvalues += " Missing STACK_UNIT_VALUE_INT (15).";
                }

                if (!stackUnitvalues.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.MAX_STACK_SIZE_INT) + Environment.NewLine + stackUnitvalues);

                }
            }
        }
        private void ARMOR_TYPE_INT()
        {
            throw new NotImplementedException();
        }

        private void DAMAGE_TYPE_INT()
        {
            //DAMAGE_TYPE_INT
            bool hasDamageType = _weenie.IntStatTryGet(IntPropertyId.DamageType);
            bool hasDamage = _weenie.IntStatTryGet(IntPropertyId.Damage);
            if (_weenie.WeenieType_Binder == WeenieType.MeleeWeapon && ((hasDamage && !hasDamageType) || (hasDamageType && !hasDamage)))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.DAMAGE_TYPE_INT));
            }
        }

        private void WEAPON_SKILL_INT_MISSING()
        {
            //WEAPON_SKILL_INT_MISSING
            bool isMeleeOrMissleWeaponType = _weenie.WeenieType_Binder == WeenieType.MeleeWeapon || _weenie.WeenieType_Binder == WeenieType.MissileLauncher || _weenie.WeenieType_Binder == WeenieType.Missile; //? true : false;
            bool hasWeaponSkillInt = _weenie.IntStatTryGet(IntPropertyId.WeaponSkill);
            if ((isMeleeOrMissleWeaponType && !hasWeaponSkillInt) || (hasWeaponSkillInt && !isMeleeOrMissleWeaponType))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.WEAPON_SKILL_INT_MISSING));
            }
        }

        private void WEAPON_SKILL_INT_OLD()
        {
            //WEAPON_SKILL_INT_OLD
            if (_weenie.IntStatTryGet(IntPropertyId.WeaponSkill, out IntStat weaponSkillStat))
            {
                switch ((SkillId)weaponSkillStat.Value)
                {
                    case SkillId.Axe:
                    case SkillId.Bow:
                    case SkillId.Crossbow:
                    case SkillId.Dagger:
                    case SkillId.Mace:
                    case SkillId.Sling:
                    case SkillId.Spear:
                    case SkillId.Staff:
                    case SkillId.Sword:
                    case SkillId.ThrownWeapon:
                    case SkillId.UnarmedCombat:
                        _issues.Add(Rules.FetchRuleInfo(Rule.WEAPON_SKILL_INT_OLD) + Environment.NewLine + $"  Skill Currently Set: {(SkillId)weaponSkillStat.Value}");
                        break;
                    default:
                        break;
                }
            }
        }

        private void ALLOW_GIVE_BOOL()
        {
            //ALLOW_GIVE_BOOL
            string exception = "";
            if (!_weenie.BoolStatTryGet(BoolPropertyId.AllowGive, out BoolStat AllowGiveBoolObject) || AllowGiveBoolObject.Value == 0)
            {
                //Does this weenie accept everything?
                if (_weenie.BoolStatTryGet(BoolPropertyId.AiAcceptEverything, true, 1))
                {
                    exception += " AI_ACCEPT_EVERYTHING_BOOL is true but ALLOW_GIVE_BOOL is false.";
                }

                //Give Emote Categories require Allow_Give bool.
                if (_weenie.EmoteTable?.FirstOrDefault(x => x.EmoteCategoryId == (int)EmoteCategory.Give) != null)
                {
                    exception += " Give Emote Categories require ALLOW_GIVE_BOOL to be true.";
                }
            }
            else
            {
                bool AiAcceptEverything = _weenie.BoolStatTryGet(BoolPropertyId.AiAcceptEverything, true, 1);
                bool giveCatPresent = _weenie.EmoteTable?.FirstOrDefault(x => x.EmoteCategoryId == (int)EmoteCategory.Give) != null ? true : false;

                //Niether condition requiring allow_give is present.
                if (!AiAcceptEverything && !giveCatPresent)
                {
                    exception += "ALLOW_GIVE_BOOL is true but all conditions requiring it do not exist. This can be safely removed.";
                }
            }

            if (!exception.Equals(""))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.ALLOW_GIVE_BOOL) + Environment.NewLine + exception);
            }
        }

        private void PROC_SPELL_DID()
        {
            //PROC_SPELL_DID
            if (_weenie.DidStatTryGet(DidPropertyId.ProcSpell) && !_weenie.FloatStatTryGet(DoublePropertyId.ProcSpellRate))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.PROC_SPELL_DID));
            }
        }

        private void SPELL_BOOK_SUPPORT_FLOATS()
        {
            switch (_weenie.WeenieType_Binder)
            {
                case WeenieType.Caster:
                case WeenieType.Clothing:
                case WeenieType.Generic:
                case WeenieType.MeleeWeapon:
                case WeenieType.MissileLauncher:
                case WeenieType.Gem:
                    {
                        bool hasManaRate = _weenie.FloatStatTryGet(DoublePropertyId.ManaRate, out FloatStat manaRate);

                        if ((_weenie.Spells != null && _weenie.Spells.Any()) || _weenie.DidStatTryGet(DidPropertyId.Spell))
                        {
                            if (_weenie.WeenieType_Binder != WeenieType.Gem)
                            {
                                if (!hasManaRate)
                                {
                                    _issues.Add(Rules.FetchRuleInfo(Rule.SPELL_BOOK_SUPPORT_FLOATS));
                                }
                                else
                                {
                                    if (manaRate.Value > 0)
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.SPELL_BOOK_SUPPORT_FLOATS) + Environment.NewLine + $"  MANA_RATE_FLOAT (5) is not a negative value. Should this item be gaining mana?");
                                    }
                                }
                            }
                        }
                        else if (hasManaRate && _weenie.WeenieType_Binder != WeenieType.Gem)
                        {
                            _issues.Add(Rules.FetchRuleInfo(Rule.SPELL_BOOK_SUPPORT_FLOATS) + Environment.NewLine + "  NO SPELLBOOK/SPELL DID PRESENT but MANA_RATE_FLOAT (5) is present. Is this deliberate?");
                        }

                        break;
                    }
                default:
                    break;
            }
        }

        private void SPELL_BOOK_SUPPORT_INTS()
        {
            switch (_weenie.WeenieType_Binder)
            {
                case WeenieType.Caster:
                case WeenieType.Clothing:
                case WeenieType.Generic:
                case WeenieType.MeleeWeapon:
                case WeenieType.MissileLauncher:
                case WeenieType.Gem:
                    {
                        bool hasItemSpellCraft = _weenie.IntStatTryGet(IntPropertyId.ItemSpellcraft);
                        bool hasCurrMana = _weenie.IntStatTryGet(IntPropertyId.ItemCurMana, out IntStat itemCurMana);
                        bool hasMaxMana = _weenie.IntStatTryGet(IntPropertyId.ItemMaxMana, out IntStat itemMaxMana);

                        if ((_weenie.Spells != null && _weenie.Spells.Any()) || _weenie.DidStatTryGet(DidPropertyId.Spell))
                        {
                            string missingIntStats = "";
                            if (!hasItemSpellCraft)
                            {
                                missingIntStats += "  Missing ITEM_SPELLCRAFT_INT (106).";
                            }
                            if (!hasCurrMana)
                            {
                                missingIntStats += "  Missing ITEM_CUR_MANA_INT (107).";
                            }
                            if (!hasMaxMana)
                            {
                                missingIntStats += "  Missing ITEM_MAX_MANA_INT (108).";
                            }
                            if ((hasCurrMana && hasMaxMana && itemCurMana.Value != itemMaxMana.Value)
                                || (hasCurrMana && !hasMaxMana)
                                || (!hasCurrMana && hasMaxMana))
                            {
                                if (!missingIntStats.Equals(""))
                                {
                                    missingIntStats += Environment.NewLine;
                                }
                                string curManaString = hasCurrMana ? itemCurMana.Value.ToString() : "NULL";
                                string maxManaString = hasMaxMana ? itemMaxMana.Value.ToString() : "NULL";
                                missingIntStats += $"  ITEM_CUR_MANA_INT (107) should be the same as ITEM_MAX_MANA_INT (108) for a weenie default. Current value: Cur:{curManaString } Max:{maxManaString}";
                            }
                            if (!missingIntStats.Equals(""))
                            {
                                _issues.Add(Rules.FetchRuleInfo(Rule.SPELL_BOOK_SUPPORT_INTS) + Environment.NewLine + missingIntStats);
                            }
                        }
                        else if (hasItemSpellCraft || hasCurrMana || hasMaxMana)
                        {
                            _issues.Add(Rules.FetchRuleInfo(Rule.SPELL_BOOK_SUPPORT_INTS) + Environment.NewLine + "  NO SPELLBOOK/SPELL DID PRESENT but some or all of the above ints are present. Is this deliberate?");
                        }

                        break;
                    }
                default:
                    break;
            }
        }

        private void GENERATOR_TABLE_INTS()
        {
            if (_weenie.HasGeneratorTable)
            {
                //GENERATOR_TABLE_INTS
                string exceptions_INTS = "";
                if (!_weenie.IntStatTryGet(IntPropertyId.InitGeneratedObjects))
                {
                    exceptions_INTS += " INIT_GENERATED_OBJECTS_INT is undefined.";
                }

                if (!_weenie.IntStatTryGet(IntPropertyId.MaxGeneratedObjects))
                {
                    exceptions_INTS += " MAX_GENERATED_OBJECTS_INT is undefined.";
                }

                if (!exceptions_INTS.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.GENERATOR_TABLE_INTS) + Environment.NewLine + exceptions_INTS);
                }
            }
        }

        private void GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT()
        {
            if (_weenie.HasGeneratorTable)
            {
                //GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT
                if (_weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)DoublePropertyId.RegenerationInterval && x.Value > 0) == null)
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.GENERATOR_TABLE_REGENERATION_INTERVAL_FLOAT));
                }
            }
        }

        private void GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT()
        {
            if (_weenie.HasGeneratorTable)
            {
                FloatStat genRadius = _weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)DoublePropertyId.GeneratorRadius);
                bool RADIUS_FLOAT_FAIL = false;
                List<uint> floatFailAndSlotsWithScatter = new List<uint>();

                foreach (GeneratorTable slot in _weenie.GeneratorTable)
                {
                    //GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT
                    if (slot.WhereCreateEnum == RegenerationLocation.Scatter || slot.WhereCreateEnum == RegenerationLocation.ScatterTreasure)
                    {
                        if (genRadius == null || genRadius.Value == 0)
                        {
                            if (genRadius?.Value == 0)
                            {
                                floatFailAndSlotsWithScatter.Add(slot.Slot);
                            }
                            RADIUS_FLOAT_FAIL = true;
                        }
                    }
                }

                if (RADIUS_FLOAT_FAIL)
                {
                    string suggestOnTopForZeroRadius = "";
                    if (floatFailAndSlotsWithScatter.Any())
                    {
                        suggestOnTopForZeroRadius += $"{Environment.NewLine} Generator radius is set to 0. Yet Generator_Table slots {string.Join(", ", floatFailAndSlotsWithScatter.ToArray())} are set to SCATTER location. ONTOP location maybe preferred.";
                    }

                    _issues.Add(Rules.FetchRuleInfo(Rule.GENERATOR_TABLE_GENERATOR_RADIUS_FLOAT) + suggestOnTopForZeroRadius);
                }

            }
        }

        private void GENERATOR_TABLE_SLOTS_SEQUENCE()
        {
            if (_weenie.HasGeneratorTable)
            {
                Dictionary<uint, int> slotNum = new Dictionary<uint, int>();
                string entrySlotIssues = "";

                foreach (GeneratorTable slot in _weenie.GeneratorTable)
                {
                    //GENERATOR_TABLE_SLOTS_SEQUENCE
                    if (!slotNum.TryAdd(slot.Slot, 1))
                    {
                        entrySlotIssues += $"{Environment.NewLine} Slot {slot.Slot} is duplicated.";
                    }
                }

                if (!entrySlotIssues.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.GENERATOR_TABLE_SLOTS_SEQUENCE) + entrySlotIssues);
                }
            }
        }

        private void GENERATOR_TABLE_PROBABILITY_SEQUENCE()
        {
            if (_weenie.HasGeneratorTable)
            {
                double slotProb = 0;
                string entryProbIssues = "";

                foreach (GeneratorTable slot in _weenie.GeneratorTable)
                {
                    //GENERATOR_TABLE_PROBABILITY_SEQUENCE

                    if (slot.Probability != -1)
                    {
                        if (slot.Probability <= slotProb)
                        {
                            entryProbIssues += $"{Environment.NewLine} Slot {slot.Slot} probability is less than or equal to previous slot.";
                        }
                        else
                        {
                            slotProb = slot.Probability;
                        }
                    }
                }

                if (!entryProbIssues.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.GENERATOR_TABLE_PROBABILITY_SEQUENCE) + entryProbIssues);
                }
            }
        }

        private void NPC_ATTACKABLE_OR_NPK()
        {
            //NPC_ATTACKABLE_OR_NPK
            if (_weenie.WeenieType_Binder == WeenieType.Vendor || _weenie.WeenieType_Binder == WeenieType.Creature)
            {
                string exceptions = "";
                bool isNPK = _weenie.IntStatTryGet(IntPropertyId.PlayerKillerStatus, true, 16);
                bool notAttackable = _weenie.BoolStatTryGet(BoolPropertyId.Attackable, true, 0);

                if (_weenie.WeenieType_Binder == WeenieType.Vendor)
                {
                    if (!isNPK)
                    {
                        exceptions += $"{Environment.NewLine} Vendor weenie missing PLAYER_KILLER_STATUS_INT (134) should be 16 - Rubber_Glue Status.";
                    }
                    if (!notAttackable)
                    {
                        exceptions += $"{Environment.NewLine} Vendor weenie requires ATTACKABLE_BOOL (19) set to false.";
                    }
                }
                else
                {
                    bool validEmoteCats = _weenie.EmoteTable?
                        .FirstOrDefault(x => x.EmoteCategoryId == (int)EmoteCategory.Refuse ||
                        x.EmoteCategoryId == (int)EmoteCategory.Give ||
                        x.EmoteCategoryId == (int)EmoteCategory.Use ||
                        x.EmoteCategoryId == (int)EmoteCategory.ReceiveLocalSignal ||
                        x.EmoteCategoryId == (int)EmoteCategory.ReceiveTalkDirect) != null ? true : false;

                    if (validEmoteCats)
                    {
                        if (!isNPK)
                        {
                            exceptions += $"{Environment.NewLine} INTERACTIVE NPC. If it should not attack players it needs PLAYER_KILLER_STATUS_INT (134) set to 16 - Rubber_Glue Status.";
                        }
                        if (!notAttackable)
                        {
                            exceptions += $"{Environment.NewLine} INTERACTIVE NPC. If weenie should be immune to damage. ATTACKABLE_BOOL (19) must be set to false.";
                        }
                    }
                    else
                    {
                        if ((isNPK || notAttackable) && !_weenie.BoolStatTryGet(BoolPropertyId.AiAcceptEverything, true, 1))
                        {
                            exceptions += $"{Environment.NewLine} Should this creature be a monster? Current weenie without valid interactive emotes has ATTACKABLE_BOOL (19) set to false OR PLAYER_KILLER_STATUS_INT (134) set to 16.";
                        }
                    }
                }
                if (!exceptions.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.NPC_ATTACKABLE_OR_NPK) + exceptions);
                }
            }
        }

        private void IS_NOT_SET_TO_VENDOR_WEENIETYPE()
        {
            //IS_SET_TO_VENDOR_WEENIETYPE
            if (_weenie.CreateList?.FirstOrDefault(x => x.Destination == (int)Destination.Shop) != null)
            {
                if (_weenie.WeenieType_Binder != WeenieType.Vendor)
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.IS_NOT_SET_TO_VENDOR_WEENIETYPE));
                }
            }
        }

        private void VENDOR_WEENIETYPE_NO_SHOP_ITEMS()
        {
            if (_weenie.WeenieType_Binder == WeenieType.Vendor)
            {
                if (_weenie.CreateList?.FirstOrDefault(x => x.Destination == (int)Destination.Shop) == null)
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.VENDOR_WEENIETYPE_NO_SHOP_ITEMS));
                }
            }
        }

        private void SHOULD_BE_STUCK()
        {
            //SHOULD_BE_STUCK
            bool validWeenieType = false;
            switch (_weenie.WeenieType_Binder)
            {
                case WeenieType.Portal:
                case WeenieType.Creature:
                case WeenieType.Admin:
                case WeenieType.Vendor:
                case WeenieType.HotSpot:
                case WeenieType.Corpse:
                case WeenieType.Cow:
                case WeenieType.Door:
                case WeenieType.Chest:
                case WeenieType.PressurePlate:
                case WeenieType.LifeStone:
                case WeenieType.Switch:
                case WeenieType.PKModifier:
                case WeenieType.ProjectileSpell:
                case WeenieType.AdvocateFane:
                case WeenieType.Sentinel:
                case WeenieType.EventCoordinator:
                case WeenieType.House:
                case WeenieType.SlumLord:
                case WeenieType.Hook:
                case WeenieType.Storage:
                case WeenieType.HousePortal:
                case WeenieType.AllegianceBindstone:
                    validWeenieType = true;
                    break;

                default:
                    break;
            }
            if (validWeenieType && !_weenie.BoolStatTryGet(BoolPropertyId.Stuck, true, 1))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.SHOULD_BE_STUCK) + Environment.NewLine + $"WeenieType: {_weenie.WeenieType_Binder}");
            }
        }

        private void GENDER_HERITAGE()
        {
            //GENDER_HERITAGE
            if (_weenie.WeenieType_Binder == WeenieType.Creature || _weenie.WeenieType_Binder == WeenieType.Vendor)
            {
                //First check some bools that will determine whether or not we care about these stats.
                bool NpcLooksLikeObject = _weenie.BoolStatTryGet(BoolPropertyId.NpcLooksLikeObject, true, 1);
                bool isEthereal = _weenie.BoolStatTryGet(BoolPropertyId.Ethereal, true, 1);

                if (!NpcLooksLikeObject && !isEthereal)
                {
                    bool hasGenderInt = _weenie.IntStatTryGet(IntPropertyId.Gender);
                    bool hasHeritageInt = _weenie.IntStatTryGet(IntPropertyId.HeritageGroup);

                    if (!hasGenderInt || !hasHeritageInt)
                    {
                        bool isValidCreatureType = false;
                        if (_weenie.IntStatTryGet(IntPropertyId.CreatureType, out IntStat creatureType))
                        {
                            switch ((CreatureType)creatureType.Value)
                            {
                                case CreatureType.Shadow:
                                case CreatureType.Human:
                                case CreatureType.Empyrean:
                                case CreatureType.AlteredHuman:
                                case CreatureType.Simulacrum:
                                    isValidCreatureType = true;
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (isValidCreatureType)
                        {
                            _issues.Add(Rules.FetchRuleInfo(Rule.GENDER_HERITAGE));
                        }
                    }
                }

            }
        }

        private void WEENIETYPE_CREATURE_DAMAGE_RATING()
        {
            //WEENIETYPE_CREATURE_DAMAGE_RATING
            if (_weenie.WeenieType_Binder == WeenieType.Creature && _weenie.IntStatTryGet(IntPropertyId.DamageRating))
            {
                _issues.Add(Rules.FetchRuleInfo(Rule.WEENIETYPE_CREATURE_DAMAGE_RATING));
            }
        }

        private void EMOTE_TABLE_PROBABILITY_SEQUENCES()
        {
            if (_weenie.EmoteTable != null)
            {
                //EMOTE_TABLE_PROBABILITY_SEQUENCES
                foreach (EmoteCategoryListing keyListing in _weenie.EmoteTable)
                {
                    if (keyListing.Emotes == null)
                    {
                        continue;
                    }

                    switch ((EmoteCategory)keyListing.EmoteCategoryId)
                    {
                        case EmoteCategory.Death:
                        case EmoteCategory.Portal:
                        case EmoteCategory.Use:
                        case EmoteCategory.Activation:
                        case EmoteCategory.Generation:
                        case EmoteCategory.PickUp:
                        case EmoteCategory.Drop:
                        case EmoteCategory.Taunt:
                        case EmoteCategory.KillTaunt:
                        case EmoteCategory.NewEnemy:
                        case EmoteCategory.Scream:
                        case EmoteCategory.Homesick:
                        case EmoteCategory.ReceiveCritical:
                        case EmoteCategory.ResistSpell:
                        case EmoteCategory.HearChat:
                        case EmoteCategory.Wield:
                        case EmoteCategory.UnWield:
                            {
                                string baseEmoteProbIssues = CheckEmoteProbSequence(keyListing.Emotes);
                                if (!baseEmoteProbIssues.Equals(""))
                                {
                                    _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + baseEmoteProbIssues);
                                }
                                break;
                            }
                        case EmoteCategory.Give:
                        case EmoteCategory.Refuse:
                            {
                                Dictionary<uint, List<Emote>> subCategoryGrouping = new Dictionary<uint, List<Emote>>();
                                foreach (Emote category in keyListing.Emotes)
                                {
                                    if (category.Category != keyListing.EmoteCategoryId)
                                    {
                                        continue;
                                    }

                                    if (subCategoryGrouping.TryGetValue((uint)category.ClassId, out List<Emote> groupList))
                                    {
                                        groupList.Add(category);
                                    }
                                    else
                                    {
                                        List<Emote> newGroupList = new List<Emote>();
                                        newGroupList.Add(category);
                                        subCategoryGrouping.Add((uint)category.ClassId, newGroupList);
                                    }
                                }

                                foreach (uint key in subCategoryGrouping.Keys)
                                {
                                    string wcidEmoteProbIssues = "";
                                    if (subCategoryGrouping.TryGetValue(key, out List<Emote> subGroup))
                                    {
                                        wcidEmoteProbIssues = CheckEmoteProbSequence(subGroup);
                                    }

                                    if (!wcidEmoteProbIssues.Equals(""))
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + $"   EmoteCatSubGroupID: WCID/ClassID:{key}  {wcidEmoteProbIssues}");
                                    }
                                }
                                break;
                            }

                        case EmoteCategory.Vendor:
                            {
                                Dictionary<uint, List<Emote>> subCategoryGrouping = new Dictionary<uint, List<Emote>>();
                                foreach (Emote category in keyListing.Emotes)
                                {
                                    if (category.Category != keyListing.EmoteCategoryId)
                                    {
                                        continue;
                                    }

                                    if (subCategoryGrouping.TryGetValue((uint)category.VendorType, out List<Emote> groupList))
                                    {
                                        groupList.Add(category);
                                    }
                                    else
                                    {
                                        List<Emote> newGroupList = new List<Emote>();
                                        newGroupList.Add(category);
                                        subCategoryGrouping.Add((uint)category.VendorType, newGroupList);
                                    }
                                }

                                foreach (uint key in subCategoryGrouping.Keys)
                                {
                                    string vendorEmoteProbIssues = "";
                                    if (subCategoryGrouping.TryGetValue(key, out List<Emote> subGroup))
                                    {
                                        vendorEmoteProbIssues = CheckEmoteProbSequence(subGroup);
                                    }

                                    if (!vendorEmoteProbIssues.Equals(""))
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + $"   EmoteCatSubGroupID: VendorId:{key}  {vendorEmoteProbIssues}");
                                    }
                                }
                                break;
                            }
                        case EmoteCategory.HeartBeat:
                            {
                                Dictionary<string, List<Emote>> subCategoryGrouping = new Dictionary<string, List<Emote>>();
                                foreach (Emote category in keyListing.Emotes)
                                {
                                    if (category.Category != keyListing.EmoteCategoryId)
                                    {
                                        continue;
                                    }

                                    if (subCategoryGrouping.TryGetValue($"{(MotionCommand)category.Style}_{(MotionCommand)category.SubStyle}", out List<Emote> groupList))
                                    {
                                        groupList.Add(category);
                                    }
                                    else
                                    {
                                        List<Emote> newGroupList = new List<Emote>();
                                        newGroupList.Add(category);
                                        subCategoryGrouping.Add($"{(MotionCommand)category.Style}_{(MotionCommand)category.SubStyle}", newGroupList);
                                    }
                                }

                                foreach (string key in subCategoryGrouping.Keys)
                                {
                                    string vendorEmoteProbIssues = "";
                                    if (subCategoryGrouping.TryGetValue(key, out List<Emote> subGroup))
                                    {
                                        vendorEmoteProbIssues = CheckEmoteProbSequence(subGroup);
                                    }

                                    if (!vendorEmoteProbIssues.Equals(""))
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + $"   EmoteCatSubGroupID: Style_SubStyle:{key}  {vendorEmoteProbIssues}");
                                    }
                                }
                                break;
                            }
                        case EmoteCategory.WoundedTaunt:
                            //TODO GDLE CURRENTLY IGNORES PROBABILITY for this data and exclusively uses health min/max. Going to include this anyways
                            {
                                Dictionary<string, List<Emote>> subCategoryGrouping = new Dictionary<string, List<Emote>>();
                                foreach (Emote category in keyListing.Emotes)
                                {
                                    if (category.Category != keyListing.EmoteCategoryId)
                                    {
                                        continue;
                                    }

                                    if (subCategoryGrouping.TryGetValue($"{category.MinHealth}_{category.MaxHealth}", out List<Emote> groupList))
                                    {
                                        groupList.Add(category);
                                    }
                                    else
                                    {
                                        List<Emote> newGroupList = new List<Emote>();
                                        newGroupList.Add(category);
                                        subCategoryGrouping.Add($"{category.MinHealth}_{category.MaxHealth}", newGroupList);
                                    }
                                }

                                foreach (string min_max in subCategoryGrouping.Keys)
                                {
                                    string vendorEmoteProbIssues = "";
                                    if (subCategoryGrouping.TryGetValue(min_max, out List<Emote> subGroup))
                                    {
                                        vendorEmoteProbIssues = CheckEmoteProbSequence(subGroup);
                                    }

                                    if (!vendorEmoteProbIssues.Equals(""))
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + $"   EmoteCatSubGroupID: Health min_max: {min_max}  {vendorEmoteProbIssues}");
                                    }
                                }
                                break;
                            }
                        case EmoteCategory.QuestSuccess:
                        case EmoteCategory.QuestFailure:
                        case EmoteCategory.TestSuccess:
                        case EmoteCategory.TestFailure:
                        case EmoteCategory.EventSuccess:
                        case EmoteCategory.EventFailure:
                        case EmoteCategory.TestNoQuality:
                        case EmoteCategory.QuestNoFellow:
                        case EmoteCategory.TestNoFellow:
                        case EmoteCategory.GotoSet:
                        case EmoteCategory.NumFellowsSuccess:
                        case EmoteCategory.NumFellowsFailure:
                        case EmoteCategory.NumCharacterTitlesSuccess:
                        case EmoteCategory.NumCharacterTitlesFailure:
                        case EmoteCategory.ReceiveLocalSignal:
                        case EmoteCategory.ReceiveTalkDirect:
                            {
                                Dictionary<string, List<Emote>> subCategoryGrouping = new Dictionary<string, List<Emote>>();
                                foreach (Emote category in keyListing.Emotes)
                                {
                                    if (category.Category != keyListing.EmoteCategoryId)
                                    {
                                        continue;
                                    }

                                    if (subCategoryGrouping.TryGetValue(category.Quest, out List<Emote> groupList))
                                    {
                                        groupList.Add(category);
                                    }
                                    else
                                    {
                                        List<Emote> newGroupList = new List<Emote>();
                                        newGroupList.Add(category);
                                        subCategoryGrouping.Add(category.Quest, newGroupList);
                                    }
                                }

                                foreach (string quest in subCategoryGrouping.Keys)
                                {
                                    string vendorEmoteProbIssues = "";
                                    if (subCategoryGrouping.TryGetValue(quest, out List<Emote> subGroup))
                                    {
                                        vendorEmoteProbIssues = CheckEmoteProbSequence(subGroup);
                                    }

                                    if (!vendorEmoteProbIssues.Equals(""))
                                    {
                                        _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_PROBABILITY_SEQUENCES) + Environment.NewLine + $"   EmoteCatSubGroupID: quest: {quest}  {vendorEmoteProbIssues}");
                                    }
                                }
                                break;
                            }
                        default:
                            _issues.Add($"Fall through on EmoteTable Checks No valid cat for: {keyListing.EmoteCategoryId}");
                            break;
                    }

                }
            }
        }

        //SUPPORT FOR RULE: EMOTE_TABLE_PROBABILITY_SEQUENCES
        private string CheckEmoteProbSequence(List<Emote> emotes)
        {
            string emoteSequenceIssue = "";
            float? currentProbability = 0;

            bool outOfSequence = false;
            EmoteCategory categoryType = EmoteCategory.Invalid;

            foreach (Emote emoteCategory in emotes)
            {
                if (emoteCategory.Probability <= currentProbability)
                {
                    outOfSequence = true;
                    categoryType = emoteCategory.EmoteCategory;
                    break;
                }
                else
                {
                    currentProbability = emoteCategory.Probability;
                }
            }

            if (outOfSequence)
            {
                emoteSequenceIssue = $"  EmoteCat:{categoryType}   Current Sequence:";
                foreach (Emote emoteCategory in emotes)
                {
                    emoteSequenceIssue += $"  {emoteCategory.Probability}";
                }
            }

            return emoteSequenceIssue;

        }

        private void EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH()
        {
            //EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH
            if (_weenie.EmoteTable != null)
            {
                foreach (EmoteCategoryListing keyListing in _weenie.EmoteTable)
                {
                    if (keyListing.Emotes == null)
                    {
                        continue;
                    }

                    foreach (Emote emoteCategory in keyListing.Emotes)
                    {
                        if (emoteCategory.Category != keyListing.EmoteCategoryId)
                        {
                            _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_KEY_CATEGORY_ID_MISMATCH) + Environment.NewLine + $" Mismatch key: {keyListing.EmoteCategoryId}");
                        }
                    }
                }
            }
        }

        private void EMOTE_TABLE_XP_REWARD()
        {
            if (_weenie.EmoteTable != null)
            {
                //EMOTE_TABLE_XP_REWARD
                string awardXpCheck = "";
                foreach (EmoteCategoryListing keyListing in _weenie.EmoteTable)
                {
                    if (keyListing.Emotes == null)
                    {
                        continue;
                    }

                    foreach (Emote emoteCategory in keyListing.Emotes)
                    {
                        if (emoteCategory.Actions == null)
                        {
                            continue;
                        }

                        foreach (EmoteAction emoteType in emoteCategory.Actions)
                        {
                            if (emoteType.EmoteActionType_Binder == EmoteType.AwardXP)
                            {
                                string catId = "NONE";
                                switch (emoteCategory.EmoteCategory)
                                {
                                    case EmoteCategory.Give:
                                    case EmoteCategory.Refuse:
                                        catId = emoteCategory.ClassId.ToString();
                                        break;
                                    case EmoteCategory.Vendor:
                                        catId = emoteCategory.VendorType.ToString();
                                        break;
                                    case EmoteCategory.HeartBeat:
                                        catId = emoteCategory.Style.ToString() + emoteCategory.SubStyle.ToString();
                                        break;
                                    case EmoteCategory.WoundedTaunt:
                                        catId = emoteCategory.MinHealth.ToString() + emoteCategory.MaxHealth.ToString();
                                        break;
                                    case EmoteCategory.QuestSuccess:
                                    case EmoteCategory.QuestFailure:
                                    case EmoteCategory.TestSuccess:
                                    case EmoteCategory.TestFailure:
                                    case EmoteCategory.EventSuccess:
                                    case EmoteCategory.EventFailure:
                                    case EmoteCategory.TestNoQuality:
                                    case EmoteCategory.QuestNoFellow:
                                    case EmoteCategory.TestNoFellow:
                                    case EmoteCategory.GotoSet:
                                    case EmoteCategory.NumFellowsSuccess:
                                    case EmoteCategory.NumFellowsFailure:
                                    case EmoteCategory.NumCharacterTitlesSuccess:
                                    case EmoteCategory.NumCharacterTitlesFailure:
                                    case EmoteCategory.ReceiveLocalSignal:
                                    case EmoteCategory.ReceiveTalkDirect:
                                        catId = emoteCategory.Quest;
                                        break;
                                    default:
                                        break;
                                }
                                awardXpCheck += $"{Environment.NewLine}    EmoteCat: {emoteCategory.EmoteCategory}  UniqueCatID: {catId}  CategoryProb: {emoteCategory.Probability}";
                            }
                        }
                    }
                }
                if (!awardXpCheck.Equals(""))
                {
                    _issues.Add(Rules.FetchRuleInfo(Rule.EMOTE_TABLE_XP_REWARD) + awardXpCheck);
                }
            }
        }

        private int AddStringPadding(int comparisonValue, ref string stringToCompare)
        {
            if (stringToCompare.Length > comparisonValue) 
            {
                stringToCompare += Environment.NewLine;
            }
            return stringToCompare.Length;
        }
    }
}
