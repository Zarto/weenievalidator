﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOLibrary;
using Lifestoned.DataModel.Gdle;

namespace Validate
{
    public class Validation_Setup
    {
        private List<Weenie> _validationList;
        private bool _alsoExportReportToFile;
        private List<string> _ConsoleWeenieReportsAggregate = new List<string>();
        private List<string> _ExportWeenieReportsAggregate = new List<string>();

        public Validation_Setup(List<Weenie> validationList, bool alsoExportReportToFile)
        {
            _validationList = validationList;
            _alsoExportReportToFile = alsoExportReportToFile;
        }


        /// <summary>
        /// Begins Validation of WeenieList provided during object construction.
        /// </summary>
        /// <param name="exceptionsFound">Boolean value indicating that all validations passed successfully (no errors detected).</param>
        /// <returns>Returns a string containing a composite report of each weenie provided for validation.</returns>
        public string ExecuteReport()
        {
            _validationList.AsParallel().ForAll(weenie => ValidateInitialize(weenie));

            if (_alsoExportReportToFile)
            {
                ExportFullReportToFile();
            }
            
            return ConsoleFullReport();
        }

        private string ConsoleFullReport()
        {
            string fullReport = "No Exceptions Found During Search.";
            if (_ConsoleWeenieReportsAggregate.Any())
            {
                fullReport = String.Join(Environment.NewLine, _ConsoleWeenieReportsAggregate.ToArray());
            }

            return fullReport;
        }

        private void ExportFullReportToFile()
        {
            string fullReport = "No Exceptions Found During Search.";
            if (_ExportWeenieReportsAggregate.Any())
            {
                fullReport = $"~~~~~File can be Indexed At~~~~~{Environment.NewLine}" +
                    $"https://docs.google.com/spreadsheets/d/1aI_cUOyncwcWvheKBmG_hLsuWRLYuS3_HKfwIcAgMzA/edit?usp=sharing" +
                    $"{Environment.NewLine}{Environment.NewLine}{Environment.NewLine}" +
                    $" {String.Join("", _ExportWeenieReportsAggregate.ToArray())}";
            }
            Export.ToFileString(fullReport,"AggregateReport.txt", ".\\Reports\\");
        }

        private void ConsoleWeenieReport(List<string> issues, Weenie weenie)
        {
            
            if (issues.Any())
            {
                string weenieReport = "";

                weenieReport += $"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{Environment.NewLine}" +
                    $"Issues Found with: {weenie.WeenieClassId} - {weenie.Name}{Environment.NewLine}{Environment.NewLine}";
                weenieReport += String.Join(Environment.NewLine + Environment.NewLine, issues.ToArray()) + Environment.NewLine;

                _ConsoleWeenieReportsAggregate.Add(weenieReport);
            }
            
        }

        private void ExportWeenieReport(List<string> issues, Weenie weenie)
        {
            if (issues.Any())
            {
                StringBuilder exportReport = new StringBuilder();
                foreach (string issue in issues)
                {
                    exportReport.Append($"{ weenie.WeenieClassId } - { weenie.Name }{issue.Replace(Environment.NewLine,"    ")}{Environment.NewLine}");
                }
                _ExportWeenieReportsAggregate.Add(exportReport.ToString());
            }         
        }

        private void ValidateInitialize(Weenie weenie)
        {            
            Validate validator = new Validate(weenie);
            List<string> issues = validator.ValidateWeenie();

            if (_alsoExportReportToFile)
            {
                ExportWeenieReport(issues, weenie);
            }
            ConsoleWeenieReport(issues, weenie);
        }
    }
}
