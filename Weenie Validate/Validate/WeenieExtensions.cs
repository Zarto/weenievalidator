﻿using System;
using System.Collections.Generic;
using System.Text;
using Lifestoned.DataModel.Gdle;
using Lifestoned.DataModel.Shared;
using System.Linq;

namespace Validate
{
    public static class WeenieExtensions
    {
        #region IntStatSearch
        /// <summary>
        /// Will get, if present in the list, the specified IntStat. If found the object will be placed in the out parameter.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an IntPropertyId</param>
        /// <param name="foundStat">The Out Value as IntStat for the found stat.</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list. Also places found stat in OUT param.</returns>
        public static bool IntStatTryGet(this Weenie weenie, IntPropertyId propID, out IntStat foundStat, bool andFindValue = false, int statValue = 0)
        {
            foundStat = null;
            if (andFindValue)
            {
                foundStat = weenie.IntStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue);
            }
            else
            {
                foundStat = weenie.IntStats?.FirstOrDefault(x => x.Key == (int)propID);
            }

            if (foundStat != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will try to find within the IntStat List the specified stat.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an IntPropertyId</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list.</returns>
        public static bool IntStatTryGet(this Weenie weenie, IntPropertyId propID, bool andFindValue = false, int statValue = 0)
        {
            if (andFindValue)
            {
                if (weenie.IntStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue) != null)
                {
                    return true;
                }
            }
            else
            {
                if (weenie.IntStats?.FirstOrDefault(x => x.Key == (int)propID) != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region FloatStatSearch
        /// <summary>
        /// Will get, if present in the list, the specified FloatStat. If found the object will be placed in the out parameter.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an FloatPropertyId</param>
        /// <param name="foundStat">The Out Value as FloatStat for the found stat.</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list. Also places found stat in OUT param.</returns>
        public static bool FloatStatTryGet(this Weenie weenie, DoublePropertyId propID, out FloatStat foundStat, bool andFindValue = false, double statValue = 0)
        {
            foundStat = null;
            if (andFindValue)
            {
                foundStat = weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue);
            }
            else
            {
                foundStat = weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)propID);
            }

            if (foundStat != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will try to find within the FloatStat List the specified stat.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an FloatPropertyId</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list.</returns>
        public static bool FloatStatTryGet(this Weenie weenie, DoublePropertyId propID, bool andFindValue = false, double statValue = 0)
        {
            if (andFindValue)
            {
                if (weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue) != null)
                {
                    return true;
                }
            }
            else
            {
                if (weenie.FloatStats?.FirstOrDefault(x => x.Key == (int)propID) != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region BoolStatSearch
        /// <summary>
        /// Will get, if present in the list, the specified BoolStat. If found the object will be placed in the out parameter.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an BoolPropertyId</param>
        /// <param name="foundStat">The Out Value as BoolStat for the found stat.</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list. Also places found stat in OUT param.</returns>
        public static bool BoolStatTryGet(this Weenie weenie, BoolPropertyId propID, out BoolStat foundStat, bool andFindValue = false, int statValue = 0)
        {
            foundStat = null;
            if (andFindValue)
            {
                foundStat = weenie.BoolStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue);
            }
            else
            {
                foundStat = weenie.BoolStats?.FirstOrDefault(x => x.Key == (int)propID);
            }

            if (foundStat != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will try to find within the BoolStat List the specified stat.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an BoolPropertyId</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list.</returns>
        public static bool BoolStatTryGet(this Weenie weenie, BoolPropertyId propID, bool andFindValue = false, int statValue = 0)
        {
            if (andFindValue)
            {
                if (weenie.BoolStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue) != null)
                {
                    return true;
                }
            }
            else
            {
                if (weenie.BoolStats?.FirstOrDefault(x => x.Key == (int)propID) != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region DiDStatSearch
        /// <summary>
        /// Will get, if present in the list, the specified DidStat. If found the object will be placed in the out parameter.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an DidPropertyId</param>
        /// <param name="foundStat">The Out Value as DidStat for the found stat.</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list. Also places found stat in OUT param.</returns>
        public static bool DidStatTryGet(this Weenie weenie, DidPropertyId propID, out DidStat foundStat, bool andFindValue = false, uint statValue = 0)
        {
            foundStat = null;
            if (andFindValue)
            {
                foundStat = weenie.DidStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue);
            }
            else
            {
                foundStat = weenie.DidStats?.FirstOrDefault(x => x.Key == (int)propID);
            }

            if (foundStat != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will try to find within the DidStat List the specified stat.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an DidPropertyId</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (==) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list.</returns>
        public static bool DidStatTryGet(this Weenie weenie, DidPropertyId propID, bool andFindValue = false, uint statValue = 0)
        {
            if (andFindValue)
            {
                if (weenie.DidStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value == statValue) != null)
                {
                    return true;
                }
            }
            else
            {
                if (weenie.DidStats?.FirstOrDefault(x => x.Key == (int)propID) != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region StringStatSearch
        /// <summary>
        /// Will get, if present in the list, the specified StringStat. If found the object will be placed in the out parameter.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an StringPropertyId</param>
        /// <param name="foundStat">The Out Value as StringStat for the found stat.</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (EXACT match) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list. Also places found stat in OUT param.</returns>
        public static bool StringStatTryGet(this Weenie weenie, StringPropertyId propID, out StringStat foundStat, bool andFindValue = false, string statValue = "")
        {
            foundStat = null;
            if (andFindValue)
            {
                foundStat = weenie.StringStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value.Equals(statValue));
            }
            else
            {
                foundStat = weenie.StringStats?.FirstOrDefault(x => x.Key == (int)propID);
            }

            if (foundStat != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Will try to find within the StringStat List the specified stat.
        /// </summary>
        /// <param name="weenie">The weenie object to check against.</param>
        /// <param name="propID">The property ID to check for as an StringPropertyId</param>
        /// <param name="andFindValue">OPTIONAL: Boolean to find not only the key but the value of that key.</param>
        /// <param name="statValue">OPTIONAL: If andFindValue is true this value will be checked for during search. If the key is found AND this value is present (EXACT match) result will be true, else false.</param>
        /// <returns> TRUE if stat is found in list.</returns>
        public static bool StringStatTryGet(this Weenie weenie, StringPropertyId propID, bool andFindValue = false, string statValue = "")
        {
            if (andFindValue)
            {
                if (weenie.StringStats?.FirstOrDefault(x => x.Key == (int)propID && x.Value.Equals(statValue)) != null)
                {
                    return true;
                }
            }
            else
            {
                if (weenie.StringStats?.FirstOrDefault(x => x.Key == (int)propID) != null)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion
    
        public static bool HasSkills(this Weenie weenie)
        {
            return weenie.Skills?.Count > 0;
        }

        public static bool HasEmoteTable(this Weenie weenie)
        {
            return weenie.EmoteTable?.Count > 0;
        }

        public static bool HasSpellTable (this Weenie weenie)
        {
            return weenie.Spells?.Count > 0;
        }

        public static bool HasAllAttributes(this Weenie weenie, out string errors)
        {
            //NOTE: A portion of this was setup such to detect 'null' fields that were getting stripped from the LSD db. 
            //The data model changed to accept nullable types, so uploaded or imported weenies have all the below null checks set to 0 if not found.
            //Going to leave this for now until a better way can be found to detect it. Ultimately the server parser should identify the missing property.
            errors = "";
            if (weenie.Attributes == null)
            {
                errors += "Attribute Table is missing.";

                // no reason to check further. Everything is missing.
                return false;
            }

            if (weenie.Attributes.Health == null)
            {
                errors += $" -Health Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Health.Ranks == null)
                {
                    errors += $" -Health Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Health.Ranks == 0)
                {
                    errors += $" -CHECK: Health Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Health.Current == null)
                {
                    errors += $" -Health Attribute - Current is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Health.LevelFromCp == null)
                {
                    errors += $" -Health Attribute - Level from CP(level_from_cp) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Health.XpSpent == null)
                {
                    errors += $" -Health Attribute - CP spent(cp_spent) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Health.Current != (weenie.Attributes.Health.Ranks + (weenie.Attributes.Endurance.Ranks / 2)))
                {
                    errors += $" -Health stat Current {weenie.Attributes.Health.Current} does not match combined max {weenie.Attributes.Health.Ranks + (weenie.Attributes.Endurance.Ranks / 2)}{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Stamina == null)
            {
                errors += $" -Stamina Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Stamina.Ranks == null)
                {
                    errors += $" -Stamina Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Stamina.Ranks == 0)
                {
                    errors += $" -CHECK: Stamina Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Stamina.Current == null)
                {
                    errors += $" -Stamina Attribute - Current is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Stamina.LevelFromCp == null)
                {
                    errors += $" -Stamina Attribute - Level from CP(level_from_cp) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Stamina.XpSpent == null)
                {
                    errors += $" -Stamina Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Stamina.Current != (weenie.Attributes.Stamina.Ranks + weenie.Attributes.Endurance.Ranks))
                {
                    errors += $" -Stamina stat Current {weenie.Attributes.Stamina.Current} does not match combined max {weenie.Attributes.Stamina.Ranks + weenie.Attributes.Endurance.Ranks}{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Mana == null)
            {
                errors += $" -Mana Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Mana.Ranks == null)
                {
                    errors += $" -Mana Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Mana.Ranks == 0)
                {
                    errors += $" -CHECK: Mana Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Mana.Current == null)
                {
                    errors += $" -Mana Attribute - Current is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Mana.LevelFromCp == null)
                {
                    errors += $" -Mana Attribute - Level from CP(level_from_cp) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Mana.XpSpent == null)
                {
                    errors += $" -Mana Attribute -  CP Spent(cp_spent) is null!{Environment.NewLine}";
                }

                if (weenie.Attributes.Mana.Current != (weenie.Attributes.Mana.Ranks + weenie.Attributes.Self.Ranks))
                {
                    errors += $" -Mana stat Current {weenie.Attributes.Mana.Current} does not match combined max {weenie.Attributes.Mana.Ranks + weenie.Attributes.Self.Ranks}{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Strength == null)
            {
                errors += $" -Strength Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Strength.Ranks == null)
                {
                    errors += $" -Strength Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Strength.Ranks == 0)
                {
                    errors += $" -Strength Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Strength.XpSpent == null)
                {
                    errors += $" -Strength Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Endurance == null)
            {
                errors += $" -Endurance Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Endurance.Ranks == null)
                {
                    errors += $" -Endurance Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Endurance.Ranks == 0)
                {
                    errors += $" -Endurance Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Endurance.XpSpent == null)
                {
                    errors += $" -Endurance Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Coordination == null)
            {
                errors += $" -Coordination Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Coordination.Ranks == null)
                {
                    errors += $" -Coordination Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Coordination.Ranks == 0)
                {
                    errors += $" -Coordination Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Coordination.XpSpent == null)
                {
                    errors += $" -Coordination Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Quickness == null)
            {
                errors += $" -Quickness Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Quickness.Ranks == null)
                {
                    errors += $" -Quickness Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Quickness.Ranks == 0)
                {
                    errors += $" -Quickness Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Quickness.XpSpent == null)
                {
                    errors += $" -Quickness Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Focus == null)
            {
                errors += $" -Focus Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Focus.Ranks == null)
                {
                    errors += $" -Focus Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Focus.Ranks == 0)
                {
                    errors += $" -Focus Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Focus.XpSpent == null)
                {
                    errors += $" -Focus Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            if (weenie.Attributes.Self == null)
            {
                errors += $" -Self Attribute is missing.{Environment.NewLine}";
            }
            else
            {
                if (weenie.Attributes.Self.Ranks == null)
                {
                    errors += $" -Self Attirbute - Ranks(init_level) is null!{Environment.NewLine}";
                }
                else if (weenie.Attributes.Self.Ranks == 0)
                {
                    errors += $" -Self Attribute is set to 0.{Environment.NewLine}";
                }

                if (weenie.Attributes.Self.XpSpent == null)
                {
                    errors += $" -Self Attribute - CP Spent(cp_spent) is null!{Environment.NewLine}";
                }
            }

            return string.IsNullOrEmpty(errors);
        }
    }
}
