﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IOLibrary;
using Lifestoned.DataModel.Gdle;
using Validate;

namespace Weenie_Validate
{
    public class Application
    {
        private List<Weenie> _weenieValidationList;
        private Load loader = new Load();
        public void start()
        {
            Config();

            string terminateOrContinue = "y";
            while (terminateOrContinue.Equals("y") || terminateOrContinue.Equals("Y"))
            {
                _weenieValidationList = loader.Begin(out string loadExceptions);

                if (!loadExceptions.Equals(""))
                {
                    Console.WriteLine($"WARNING: Some files/weenies encountered load errors:{Environment.NewLine}{loadExceptions}");
                }
                if (Global.DisplayLoadTimers)
                {
                    Console.WriteLine($"=================={Environment.NewLine}{loader.LoadTimers}{Environment.NewLine}");
                }
                Console.WriteLine($"=================={Environment.NewLine}Total Files loaded: {_weenieValidationList.Count} {Environment.NewLine}");

                Console.WriteLine("Press any key to continue validation.");
                Console.ReadKey();

                Validation_Setup validate = new Validation_Setup(_weenieValidationList, GlobalConfig.ExportAggregateReport);
                string report = validate.ExecuteReport();

                Console.WriteLine(Environment.NewLine + report);
                Console.WriteLine($"{Environment.NewLine}Press y to reload and scan again. Press any other key to exit.");
                terminateOrContinue = Console.ReadLine();
            }
           
        }

        private void Config()
        {
            if (GlobalConfig.ExportRuleSet)
            {
                string ruleSet = String.Join(Environment.NewLine, EnumUtil.GetValues<Rule>().Select(rule => Rules.FetchRuleInfo(rule)).ToArray());
                Export.ToFileString(ruleSet,"RuleSet.txt", ".\\Reports\\");
            }

        }
    }
}
