﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weenie_Validate
{
    public static class Global
    {
        public const string version = "1.36";
        public const bool DisplayLoadTimers = true;
    }

    public static class GlobalConfig
    {
        //TODO: Import these values from file or the configuration assembly...
        public static bool ExportAggregateReport { get; set; } = true;
        public static bool ExportRuleSet { get; set; } = true;
    }
}

