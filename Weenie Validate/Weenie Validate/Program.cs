﻿using System;

namespace Weenie_Validate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Welcome to Weenie Validate. V{Global.version}");
            Console.WriteLine($"Loading...please wait...");
            Application weenieValidate = new Application();
            weenieValidate.start();
        }
    }
}
